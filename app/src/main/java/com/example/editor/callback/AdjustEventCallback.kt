package com.example.editor.callback

import com.example.editor.event.AdjustEvent
import com.example.editor.event.AdjustItemEvent

interface AdjustEventCallback {
    fun onAdjustClicked(event : AdjustEvent)
    fun onAdjustItemSelected(event : AdjustItemEvent)
}