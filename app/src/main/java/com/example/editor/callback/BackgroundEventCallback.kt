package com.example.editor.callback

import com.example.editor.event.BackgroundEvent

interface BackgroundEventCallback {
    fun onBackgroundClicked(event : BackgroundEvent)
}