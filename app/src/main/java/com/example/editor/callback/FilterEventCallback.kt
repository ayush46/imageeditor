package com.example.editor.callback

import com.example.editor.event.FilterEvent
import com.example.editor.event.FilterImageEvent

interface FilterEventCallback {
    fun onFilterClicked(event : FilterEvent)
    fun onFilterItemSelected(event : FilterImageEvent)
}