package com.example.editor.callback

import com.example.editor.event.CropEvent

interface CropEventCallback {
    fun onCropClicked(event : CropEvent)
}