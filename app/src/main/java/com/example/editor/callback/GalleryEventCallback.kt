package com.example.editor.callback

import com.example.editor.event.GalleryCategoryEvent
import com.example.editor.event.GalleryImageEvent

interface GalleryEventCallback {
    fun onGalleryImageClicked(event : GalleryImageEvent)
    fun onGalleryCategoryClicked(event : GalleryCategoryEvent)
}