package com.example.editor.callback

import android.net.Uri

interface OnMediaItemSelected {
    public fun onMediaImageItemSelected(position: Int)
    public fun onMediaImageItemSelected(uri: Uri)
    public fun onMediaAlbumSelected(buckets: Array<String?>)
    public fun onMediaAlbumSelected(name: String)
    public fun onMediaAlbumSelected(position: Int)
}