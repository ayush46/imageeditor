package com.example.editor.callback

import com.example.editor.event.CropModeEvent

interface CropModeCallback  {
    fun onCropModeSelected(event : CropModeEvent)
}