package com.example.editor.callback

import com.example.editor.event.RotateEvent
import com.example.editor.event.RotateItemEvent

interface RotateEventCallback {
    fun onRotateClicked(event : RotateEvent)
    fun onRotateItemSelected(event : RotateItemEvent)
}