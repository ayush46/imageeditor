package com.example.editor.viewmodel

import android.app.Application
import android.content.ContentValues
import android.graphics.Bitmap
import android.net.Uri
import android.provider.MediaStore
import com.example.editor.view.EditActivity
import com.zomato.photofilters.FilterPack
import com.zomato.photofilters.utils.ThumbnailItem
import com.zomato.photofilters.utils.ThumbnailsManager

class EditViewModel(application: Application) : BaseViewModel(application) {

    private val mApplication = application

    fun saveImage(path : String) : Uri? {
        val values = ContentValues()

        values.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis())
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg")
        values.put(MediaStore.MediaColumns.DATA, path)

        return mApplication.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)
    }

    fun prepareFilterThumbnail(mBitmap: Bitmap?, prepareFilterListener: EditActivity.PrepareFilterListener?) {
        val thumbnailItemList : ArrayList<ThumbnailItem>? = ArrayList()

        val r = Runnable {
            val thumbImage: Bitmap? = mBitmap

            if (thumbImage == null)
                return@Runnable

            ThumbnailsManager.clearThumbs()
            thumbnailItemList?.clear()

            // add normal mBitmap first
            val thumbnailItem = ThumbnailItem()
            thumbnailItem.image = thumbImage
            thumbnailItem.filterName = "Normal"
            ThumbnailsManager.addThumb(thumbnailItem)

            val filters = FilterPack.getFilterPack(mApplication)

            for (filter in filters) {
                val tI = ThumbnailItem()
                tI.image = thumbImage
                tI.filter = filter
                tI.filterName = filter.getName()
                ThumbnailsManager.addThumb(tI)
            }
            if (thumbnailItemList != null) {
                thumbnailItemList.addAll(ThumbnailsManager.processThumbs(mApplication))
            }
            prepareFilterListener?.onFilterPrepared(thumbnailItemList)
        }

        Thread(r).start()
    }

}