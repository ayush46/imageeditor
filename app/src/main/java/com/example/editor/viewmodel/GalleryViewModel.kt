package com.example.editor.viewmodel

import android.app.Application
import android.database.Cursor
import android.net.Uri
import android.provider.MediaStore
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlin.coroutines.CoroutineContext

class GalleryViewModel(application : Application) : BaseViewModel(application), CoroutineScope {

    private val mApplication = application
    private val job = Job()
    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main


    private var imagesLiveData: MutableLiveData<ArrayList<String>> = MutableLiveData()
    private val albumMap : HashMap<String, ArrayList<String>> = HashMap()

    fun getImageList() : MutableLiveData<ArrayList<String>> {
        return imagesLiveData
    }

    fun getGalleryImages(){
       /* launch(Dispatchers.Main) {
            imagesLiveData.value = withContext(Dispatchers.IO) {
                loadImagesfromGallery()
            }
        }*/
        imagesLiveData.value = loadImagesfromGallery()
    }

    private fun loadImagesfromGallery(): ArrayList<String> {
        val uri: Uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        var cursor: Cursor? = null
        var column_index_data: Int
        var absolutePathOfImage: String
        val imageList = ArrayList<String>()
        val projection = arrayOf(MediaStore.MediaColumns.DATA, MediaStore.Images.Media.BUCKET_DISPLAY_NAME)

        cursor = mApplication.contentResolver.query(uri, projection, null, null, MediaStore.Images.ImageColumns.DATE_TAKEN + " DESC")
        column_index_data = cursor?.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA)!!
        while (cursor.moveToNext()) {
            val bucketName = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.BUCKET_DISPLAY_NAME))
            absolutePathOfImage = cursor.getString(column_index_data)
            if(albumMap.containsKey(bucketName) && albumMap.get(bucketName) != null) {
                albumMap.get(bucketName)?.add(absolutePathOfImage)
            } else {
                val tempList = ArrayList<String>()
                tempList.add(absolutePathOfImage)
                albumMap.put(bucketName, tempList)
            }
            imageList.add(absolutePathOfImage)
        }
        return imageList
    }

    fun getGalleryCategoryList() : HashMap<String, ArrayList<String>>?{
        return albumMap
    }

}