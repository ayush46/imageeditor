package com.example.editor.constant

class Constants {

    companion object {
        val FILE_PROVIDER = "com.example.editor.provider"

        //Bottomsheet Tag
        val TAG_GALLERY_CATEGORY = "GALLERY_CATEGORY_DIALOG"

        //Intent keys
        val IMAGE_PATH = "imagePath"
        val GALLERY_CATEGORY = "categories"
        val BACKGROUND_REFERRER = "background"

    }

    enum class GalleryEvent {
        GALLERY_CATEGORY_SELECTION_EVENT, GALLERY_IMAGE_SELECTION_EVENT
    }

    enum class FilterEvent{
        FILTER_EVENT, FILTER_ITEM_SELECTION
    }

    enum class AdjustEvent{
        ADJUST_EVENT, ADJUST_ITEM_SELECTION
    }

    enum class CropEvent{
        CROP_EVENT, CROP_MODE_SELECTION
    }

    enum class RotateEvent{
        ROTATE_EVENT, ROTATE_ITEM_SELECTION
    }

    enum class BackgroundEvent{
        BACKGROUND_EVENT
    }

    enum class EditToolType {
        CANVAS, EDIT, FILTER, ADJUST, CROP, TEXT, ROTATE, BACKGROUND
    }

    enum class AdjustItems(val ID : Int) {
        LIGHTNESS(10), CONTRAST(20), WRAMTH(30), SATURATION(40), FADE(50), SHADOW(60), HIGHLIGHT(70)
    }

    enum class RotateItems(val ID : Int) {
        FLIP(11), ROTATE(12), ANGEL(13), ZOOM(14)
    }

    enum class Filter {
        NONE, AUTO_FIX, BLACK_WHITE, BRIGHTNESS, CONTRAST, CROSS_PROCESS, DOCUMENTARY, DUE_TONE, FILL_LIGHT, FISH_EYE,
        FLIP_VERTICAL, FLIP_HORIZONTAL, GRAIN, GRAY_SCALE, LOMISH, NEGATIVE, POSTERIZE, ROTATE, SATURATE,
        SEPIA, SHARPEN, TEMPERATURE, TINT, VIGNETTE
    }
}