package com.example.editor.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.editor.R
import com.example.editor.adapter.GalleryCategoryAdapter
import com.example.editor.constant.Constants
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class GalleryCategoryFragment : BottomSheetDialogFragment() {

    private var categories : HashMap<String, ArrayList<String>>? = null

    companion object {
        fun newInstance(categories : HashMap<String, ArrayList<String>>?) : GalleryCategoryFragment{
            val galleryCategoryFragment = GalleryCategoryFragment()
            val bundle = Bundle()
            bundle.putSerializable(Constants.GALLERY_CATEGORY, categories)
            galleryCategoryFragment.arguments = bundle
            return galleryCategoryFragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        categories = arguments?.getSerializable(Constants.GALLERY_CATEGORY) as HashMap<String, ArrayList<String>>?
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val mView = inflater.inflate(R.layout.fragment_gallery_category, container)

        if(mView != null){
            val mRvCategory = mView.findViewById<RecyclerView>(R.id.rv_cat)

            categories?.let {
                mRvCategory.layoutManager = LinearLayoutManager(mRvCategory.context, RecyclerView.VERTICAL, false)
                mRvCategory.adapter = GalleryCategoryAdapter(it)
                mRvCategory.setHasFixedSize(true)
            }
        }
        super.onCreateView(inflater, container, savedInstanceState)
        return mView
    }

}