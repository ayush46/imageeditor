package com.example.editor.view

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.editor.GalleryItemDecorator
import com.example.editor.R
import com.example.editor.adapter.GalleryAdapter
import com.example.editor.callback.GalleryEventCallback
import com.example.editor.constant.Constants
import com.example.editor.event.EventBus
import com.example.editor.event.GalleryCategoryEvent
import com.example.editor.event.GalleryImageEvent
import com.example.editor.utils.Utils
import com.example.editor.viewmodel.GalleryViewModel
import com.example.editor.viewmodel.ViewModelFactory
import org.greenrobot.eventbus.Subscribe

class GalleryFragment() : BaseFragment(), View.OnClickListener, GalleryEventCallback {

    private val REQUEST_CODE = 1001
    private val IMAGE_PICK_CODE: Int = 1002
    private var mGalleryViewModel : GalleryViewModel? = null
    private var mView : View? = null
    private var ctx : Context? = null
    private var mRecyclerView : RecyclerView? = null
    private var tvCategory : TextView? = null
    private var ivMore : ImageView? = null
    private var galleryAdapter : GalleryAdapter? = null
    private var reference : String? = null

    companion object {

        fun getInstance(ref: String?): GalleryFragment {
            val fragment = GalleryFragment()
            val bundle = Bundle()
            bundle.putString(Constants.BACKGROUND_REFERRER, ref)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        ctx = context
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViewModel()
        reference = arguments?.getString(Constants.BACKGROUND_REFERRER) as String?
        EventBus.getInstance().register(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mView = inflater.inflate(R.layout.fragment_gallery, container, false)
        mRecyclerView = mView?.findViewById(R.id.rv_image)
        tvCategory = mView?.findViewById(R.id.tv_gal_cat)
        ivMore = mView?.findViewById(R.id.iv_more)
        tvCategory?.setOnClickListener(this)
        ivMore?.setOnClickListener(this)
        return mView
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if(ctx?.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){
            fetchImageFromGallery()
        } else {
            val permissions = arrayOf<String>(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)
            this.requestPermissions(permissions, REQUEST_CODE)
        }
    }

    private fun initViewModel() {
        mGalleryViewModel = ViewModelProviders.of(this, activity?.application?.let { ViewModelFactory(it) }).get(GalleryViewModel::class.java)
    }

    private fun fetchImageFromGallery() {
        mGalleryViewModel?.getImageList()?.observe(this, Observer<ArrayList<String>> {
            Log.d("Image List", it.toString())
            galleryAdapter = GalleryAdapter(it)
            mRecyclerView?.setHasFixedSize(true)
            mRecyclerView?.layoutManager = GridLayoutManager(ctx, 4)
            mRecyclerView?.addItemDecoration(GalleryItemDecorator(ctx, R.dimen.xs))
            mRecyclerView?.adapter = galleryAdapter
        })
        mGalleryViewModel?.getGalleryImages()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(requestCode == REQUEST_CODE) {
            if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.d("Permission", "Permission: " + permissions[0] + "was " + grantResults[0])
                fetchImageFromGallery()
            }
        }
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.tv_gal_cat ->{
                val categoryMap = mGalleryViewModel?.getGalleryCategoryList()
                GalleryCategoryFragment.newInstance(categoryMap).show(childFragmentManager, Constants.TAG_GALLERY_CATEGORY)
            }

            R.id.iv_more -> {
                getImagePickAction()
            }
        }
    }

    private fun getImagePickAction() {
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, IMAGE_PICK_CODE)
    }

    @Subscribe
    override fun onGalleryCategoryClicked(event: GalleryCategoryEvent) {
        tvCategory?.text = event.category
        galleryAdapter?.updateData(event.imageList)
        val fragment = childFragmentManager.findFragmentByTag(Constants.TAG_GALLERY_CATEGORY)
        fragment.let { Utils.hideBottomSheetFragment(it) }
    }

    @Subscribe
    override fun onGalleryImageClicked(event: GalleryImageEvent) {
        if(reference != null){
            val intent = Intent()
            activity?.setResult(Activity.RESULT_OK, intent.setType(event.imagePath))
            activity?.finish()
        } else {
            val intent = Intent(ctx, EditActivity::class.java)
            intent.putExtra(Constants.IMAGE_PATH, event.imagePath)
            startActivity(intent)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                IMAGE_PICK_CODE -> {
                    val uri = data?.data
                    val intent = Intent(ctx, EditActivity::class.java)
                    if(uri != null) {
                        intent.putExtra(Constants.IMAGE_PATH, Utils.getImagePathFromUri(ctx!!, uri))
                        startActivity(intent)
                    }
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getInstance().unregister(this)
    }
}