package com.example.editor.view

import android.app.Activity
import android.content.Context
import androidx.fragment.app.Fragment
import com.example.editor.event.EventBus

open class BaseFragment : Fragment() {

    private var mActivity : Activity? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is Activity) {
            this.mActivity = context
        }
        EventBus.getInstance().register(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getInstance().unregister(this)
    }
}