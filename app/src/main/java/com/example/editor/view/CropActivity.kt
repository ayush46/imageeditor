package com.example.editor.view

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.editor.R
import com.example.editor.adapter.CropAdapter
import com.example.editor.callback.CropModeCallback
import com.example.editor.constant.Constants
import com.example.editor.event.CropModeEvent
import com.example.editor.event.EventBus
import com.example.editor.utils.Utils
import com.example.editor.viewmodel.EditViewModel
import com.isseiaoki.simplecropview.CropImageView
import com.isseiaoki.simplecropview.callback.CropCallback
import com.isseiaoki.simplecropview.callback.LoadCallback
import com.isseiaoki.simplecropview.callback.SaveCallback
import org.greenrobot.eventbus.Subscribe
import java.io.File

class CropActivity : BaseActivity(), LoadCallback, CropCallback, CropModeCallback, View.OnClickListener {

    private lateinit var ivCrop : CropImageView
    private var imgPath : String? = null
    private var rvCropMode : RecyclerView? = null
    private var llSave : LinearLayout? = null
    private var llFilterAdjustView : LinearLayout? = null
    private var ivSave : ImageView? = null
    private var ivClose : ImageView? = null
    private var tvTitle : TextView? = null
    private var file : File? = null
    private var uri : Uri? = null
    private var mEditViewModel : EditViewModel? = null
    private val mCompressFormat = Bitmap.CompressFormat.PNG
    private var cropAdapter : CropAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_crop)
        imgPath = intent.getStringExtra(Constants.IMAGE_PATH)
        file = File(imgPath)
        uri = Uri.fromFile(file)
        initView()
        initViewModel()
        EventBus.getInstance().register(this)
    }

    private fun initView() {
        ivCrop = findViewById(R.id.iv_crop)
        rvCropMode = findViewById(R.id.rv_crop_mode)
        llSave = findViewById(R.id.ll_save)
        llFilterAdjustView = findViewById(R.id.ll_filter_adjust)
        ivSave = findViewById(R.id.ivsave)
        ivClose = findViewById(R.id.iv_close)
        tvTitle = findViewById(R.id.tv_title)

        ivSave?.setOnClickListener(this)
        ivClose?.setOnClickListener(this)

        ivClose?.visibility = View.VISIBLE
        tvTitle?.text = ""

        ivCrop.load(Uri.fromFile(File(imgPath))).execute(this)

        val cropList = Utils.getCropModeDataSet()
        val cropModeLayoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        rvCropMode?.layoutManager = cropModeLayoutManager
        rvCropMode?.setHasFixedSize(true)
        cropAdapter = CropAdapter(cropList)
        rvCropMode?.adapter = cropAdapter

    }

    private fun initViewModel() {
        mEditViewModel = ViewModelProviders.of(this).get(EditViewModel::class.java)
    }

    @Subscribe
    override fun onCropModeSelected(event: CropModeEvent) {
        Log.d("selected", "Crop mode selected")
        ivCrop.setCropMode(event.cropMode)
        cropAdapter?.setCropMode(event.cropMode)
        llSave?.visibility = View.VISIBLE
    }

    override fun onClick(view: View?) {
        when(view?.id){
            R.id.ivsave -> saveImage()
            R.id.iv_close -> discardChanges()
        }
    }

    private fun discardChanges() {
        finish()
    }

    private fun saveImage() {
        ivCrop.crop(uri).execute(object : CropCallback {
            override fun onSuccess(cropped: Bitmap?) {
                ivCrop.save(cropped)
                        .compressFormat(mCompressFormat)
                        .execute(createSaveUri(), object : SaveCallback{
                            override fun onSuccess(uri: Uri?) {
                                val intent = Intent()
                                intent.setData(uri)
                                setResult(Activity.RESULT_OK, intent)
                                finish()
                            }

                            override fun onError(e: Throwable?) {
                                Log.d("SAVE_IMAGE", "Image not saved")
                            }

                        })
            }

            override fun onError(e: Throwable?) {
                Log.d("SAVE_IMAGE", "Not abel to execute saving the image")
            }

        })
    }

    fun createSaveUri(): Uri? {
        val path = Environment.getExternalStorageDirectory().toString() + File.separator + "" + System.currentTimeMillis() + ".png"
        return mEditViewModel?.saveImage(path)
    }

    //on success loading of the image on CropImageView
    override fun onSuccess() {
        Log.d("Load", "Successfully loaded image")
    }

    override fun onSuccess(cropped: Bitmap?) {
        Log.d("cropped", "successfully cropped the image")
    }

    override fun onError(e: Throwable?) {
        Log.d("cropped", "not able to crop the image")
    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getInstance().unregister(this)
    }

}