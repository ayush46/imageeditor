package com.example.editor.view

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.TransformationUtils.rotateImage
import com.example.editor.R
import com.example.editor.adapter.*
import com.example.editor.callback.*
import com.example.editor.constant.Constants
import com.example.editor.event.*
import com.example.editor.utils.Utils
import com.example.editor.viewmodel.EditViewModel
import com.nnacres.app.extensions.flip
import com.zomato.photofilters.utils.ThumbnailItem
import ja.burhanrashid52.photoeditor.OnSaveBitmap
import ja.burhanrashid52.photoeditor.PhotoEditor
import ja.burhanrashid52.photoeditor.PhotoEditorView
import ja.burhanrashid52.photoeditor.SaveSettings
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.Subscribe
import java.io.File

class EditActivity : BaseActivity(), FilterEventCallback, OnSaveBitmap, AdjustEventCallback,
        SeekBar.OnSeekBarChangeListener, CropEventCallback, RotateEventCallback, BackgroundEventCallback,
        View.OnClickListener, View.OnTouchListener, BackgroundColorAdapter.ClickOnBackgroundItem, BlurBackgroundAdapter.ClickOnBlurBackGround {

    private val READ_WRITE_STORAGE = 52
    private val CROP_IMAGE = 1001
    private val SELECT_BACKGROUND = 1002
    private val FINAL_SAVING_REQUEST = 1003

    companion object {
        val BACKGROUND_REQUEST = 55
    }
    private var imgPath : String? = null
    private var photoEditorView : PhotoEditorView? = null
    private var rvBottomNav : RecyclerView? = null
    private var rvFilter : RecyclerView? = null
    private var rvAdjust : RecyclerView? = null
    private var rvRotate : RecyclerView? = null
    private var rvBackgroundColor : RecyclerView? = null
    private var rvBlurBackground : RecyclerView? = null
    private var mPhotoEditor : PhotoEditor? = null
    private var mBitmap : Bitmap? = null
    private var changedBitmap : Bitmap? = null
    private var bottomNavView : LinearLayout? = null
    private var filterView : LinearLayout? = null
    private var adjustView : LinearLayout? = null
    private var rotateView : LinearLayout? = null
    private var backgroundView : LinearLayout? = null
    private var sbBrightness : SeekBar? = null
    private var sbContrast : SeekBar? = null
    private var sbWramth : SeekBar? = null
    private var sbSaturation : SeekBar? = null
    private var sbAngle : SeekBar? = null
    private var sbZoom : SeekBar? = null
    private var tvFilter : TextView? = null
    private var tvAdjust : TextView? = null
    private var tvTitle : TextView? = null
    private var tvBckgTitle : TextView? = null

    private var llViews : LinearLayout? = null
    private var llSave : LinearLayout? = null
    private var rlBack : RelativeLayout? = null
    private var ivToggle : ImageView? = null
    private var ivFilterSave : ImageView? = null

    // to be use to differentiate the save for filter/adjust & others
    private var ivSave : ImageView? = null
    //to be use to save Background Image
    private var saveBckg : ImageView? = null

    private var ivClose : ImageView? = null
    private var ivBack : ImageView? = null
    private var tvSave : TextView? = null

    private var typeId : Int = -1
    private var lightnessValue : Float = 1f
    private var contrastValue : Float = 1f
    private var temperatureValue : Float = 0f
    private var saturationValue : Float = 0.9f

    private var mEditViewModel : EditViewModel? = null
    private var backgroundColorAdapter : BackgroundColorAdapter? = null
    private var blurBackgroundAdapter : BlurBackgroundAdapter? = null

    private var rotateAdapter : RotateAdapter? = null
    private var adjustAdapter : AdjustAdapter? = null

    private var filterData : ArrayList<ThumbnailItem>? = null
    private var SEND_PATH_OTHER_APP : String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit)
        imgPath = intent.getStringExtra(Constants.IMAGE_PATH)
        initViews()
        initViewModel()
        loadImage()
        EventBus.getInstance().register(this)
    }

    private fun initViews() {
        bottomNavView = findViewById(R.id.bottom_nav)
        filterView = findViewById(R.id.filter_view)
        adjustView = findViewById(R.id.adjust_view)
        rotateView = findViewById(R.id.rotate_view)
        backgroundView = findViewById(R.id.background_view)
        sbBrightness = findViewById(R.id.sb_light)
        sbContrast = findViewById(R.id.sb_contrast)
        sbWramth = findViewById(R.id.sb_wramth)
        sbSaturation = findViewById(R.id.sb_saturation)
        sbAngle = findViewById(R.id.sb_angle)
        sbZoom = findViewById(R.id.sb_zoom)

        tvFilter = findViewById(R.id.tv_filter)
        tvAdjust = findViewById(R.id.tv_adjust)
        tvTitle = findViewById(R.id.tv_title)
        tvBckgTitle = findViewById(R.id.tvtitle)

        llViews = findViewById(R.id.ll_views)
        llSave = findViewById(R.id.ll_save)
        rlBack = findViewById(R.id.ll_back)
        ivToggle = findViewById(R.id.iv_toggle)
        ivFilterSave = findViewById(R.id.iv_save)
        ivSave = findViewById(R.id.ivsave)
        ivClose = findViewById(R.id.iv_close)
        ivBack = findViewById(R.id.iv_back)
        tvSave = findViewById(R.id.tv_save)
        saveBckg = findViewById(R.id.save)

        photoEditorView = findViewById(R.id.iv_edit)
        mPhotoEditor = PhotoEditor.Builder(this, photoEditorView).build()

        rvBottomNav = findViewById(R.id.rv_edit)
        val bottomItemList = Utils.getBottomNavDataSet(this)
        val bottomNavLayoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        rvBottomNav?.layoutManager = bottomNavLayoutManager
        rvBottomNav?.adapter = BottomNavAdapter(bottomItemList)

        rvFilter = findViewById(R.id.rv_filter)
        rvAdjust = findViewById(R.id.rv_adjust)
        rvRotate = findViewById(R.id.rv_rotate)
        rvBackgroundColor = findViewById(R.id.rv_background_color)
        rvBlurBackground = findViewById(R.id.rv_background_blur)

        sbBrightness?.setOnSeekBarChangeListener(this)
        sbContrast?.setOnSeekBarChangeListener(this)
        sbWramth?.setOnSeekBarChangeListener(this)
        sbSaturation?.setOnSeekBarChangeListener(this)
        sbAngle?.setOnSeekBarChangeListener(this)
        sbZoom?.setOnSeekBarChangeListener(this)

        tvFilter?.setOnClickListener(this)
        tvAdjust?.setOnClickListener(this)
        ivFilterSave?.setOnClickListener(this)
        ivSave?.setOnClickListener(this)
        ivClose?.setOnClickListener(this)
        ivBack?.setOnClickListener(this)
        tvSave?.setOnClickListener(this)
        saveBckg?.setOnClickListener(this)

        ivToggle?.setOnTouchListener(this)

        tvFilter?.setTextAppearance(this, R.style.TextView_xl)
        tvAdjust?.setTextAppearance(this, R.style.TextView_xl)

    }

    private fun loadImage() {
        photoEditorView?.source?.let { Glide.with(photoEditorView?.context!!).load(imgPath).into(it) }

        GlobalScope.launch {
            mBitmap = BitmapFactory.decodeFile(imgPath)
            System.loadLibrary("NativeImageProcessor")
            mEditViewModel?.prepareFilterThumbnail(mBitmap, object : PrepareFilterListener{
                override fun onFilterPrepared(filterItemList: ArrayList<ThumbnailItem>?) {
                    filterData = filterItemList
                    initFilterAdapter()
                }
            })
        }
    }

    private fun initFilterAdapter() {
        val filterLayoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        rvFilter?.layoutManager = filterLayoutManager
        rvFilter?.setHasFixedSize(true)
        rvFilter?.adapter = FilterAdapter(filterData)
    }

    private fun initViewModel() {
        mEditViewModel = ViewModelProviders.of(this).get(EditViewModel::class.java)
    }

    @Subscribe
    override fun onFilterClicked(event: FilterEvent) {
        showFilterView()
    }

    @Subscribe
    override fun onFilterItemSelected(event: FilterImageEvent) {
        val filter = event.filter
        val filterImage = mBitmap?.copy(Bitmap.Config.ARGB_8888, true)
        photoEditorView?.source?.setImageBitmap(filter?.processFilter(filterImage))
    }

    @Subscribe
    override fun onAdjustClicked(event: AdjustEvent) {
        showAdjustView()
    }

    @Subscribe
    override fun onAdjustItemSelected(event: AdjustItemEvent) {
        when(event.adjustType) {
            Constants.AdjustItems.LIGHTNESS.name ->{
                sbBrightness?.visibility = View.VISIBLE
                sbContrast?.visibility = View.GONE
                sbWramth?.visibility = View.GONE
                sbSaturation?.visibility = View.GONE
                typeId = Constants.AdjustItems.LIGHTNESS.ID
            }
            Constants.AdjustItems.CONTRAST.name ->{
                sbContrast?.visibility = View.VISIBLE
                sbBrightness?.visibility = View.GONE
                sbWramth?.visibility = View.GONE
                sbSaturation?.visibility = View.GONE
                typeId = Constants.AdjustItems.CONTRAST.ID
            }
            Constants.AdjustItems.WRAMTH.name ->{
                sbWramth?.visibility = View.VISIBLE
                sbBrightness?.visibility = View.GONE
                sbContrast?.visibility = View.GONE
                sbSaturation?.visibility = View.GONE
                typeId = Constants.AdjustItems.WRAMTH.ID
            }
            Constants.AdjustItems.SATURATION.name ->{
                sbSaturation?.visibility = View.VISIBLE
                sbBrightness?.visibility = View.GONE
                sbContrast?.visibility = View.GONE
                sbWramth?.visibility = View.GONE
                typeId = Constants.AdjustItems.SATURATION.ID
            }
        }
        adjustAdapter?.notifySelectedItem(event.position)
    }

    @Subscribe
    override fun onBackgroundClicked(event: BackgroundEvent) {
        llViews?.visibility = View.GONE
        showBackgroundView()
    }

    private fun showBackgroundView() {
        rlBack?.visibility = View.GONE
        bottomNavView?.visibility = View.GONE
        backgroundView?.visibility = View.VISIBLE
        tvBckgTitle?.text = "Background"

        val blurBackgroundLayoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        rvBlurBackground?.layoutManager = blurBackgroundLayoutManager
        rvBlurBackground?.setHasFixedSize(true)
        blurBackgroundAdapter = BlurBackgroundAdapter(this, this, mBitmap)
        rvBlurBackground?.adapter = blurBackgroundAdapter

        val colorBackgroundLayoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        rvBackgroundColor?.layoutManager = colorBackgroundLayoutManager
        rvBackgroundColor?.setHasFixedSize(true)
        backgroundColorAdapter = BackgroundColorAdapter(this, this)
        rvBackgroundColor?.adapter = backgroundColorAdapter

        backgroundColorAdapter?.notifyColorList("modern")

    }

    @Subscribe
    override fun onCropClicked(event: CropEvent) {
        llViews?.visibility = View.GONE
        val intent = Intent(this, CropActivity::class.java)
        intent.putExtra(Constants.IMAGE_PATH, imgPath)
        startActivityForResult(intent,CROP_IMAGE)
    }

    @Subscribe
    override fun onRotateClicked(event: RotateEvent) {
        llViews?.visibility = View.GONE
        showRotateView()
    }

    private fun showRotateView() {
        rlBack?.visibility = View.GONE
        bottomNavView?.visibility = View.GONE
        rotateView?.visibility = View.VISIBLE
        tvTitle?.text = "Rotate"

        val rotateLayoutManager = GridLayoutManager(this, 4)
        rvRotate?.layoutManager = rotateLayoutManager
        rvRotate?.setHasFixedSize(true)
        rotateAdapter = RotateAdapter(Utils.getRotateDataSet(this))
        rvRotate?.adapter = rotateAdapter
    }

    @Subscribe
    override fun onRotateItemSelected(event: RotateItemEvent) {
        when(event.rotateType){
            Constants.RotateItems.FLIP.name -> flipHorizontally()
            Constants.RotateItems.ROTATE.name -> rotateImg(90)
            Constants.RotateItems.ANGEL.name -> {
                sbAngle?.visibility = View.VISIBLE
                sbZoom?.visibility = View.GONE
                typeId = Constants.RotateItems.ANGEL.ID
            }
            Constants.RotateItems.ZOOM.name -> {
                sbZoom?.visibility = View.VISIBLE
                sbAngle?.visibility = View.GONE
                typeId = Constants.RotateItems.ZOOM.ID
            }
        }
        rotateAdapter?.notifySelectedItem(event.pos)
    }

    private fun flipHorizontally() {
        val cx = mBitmap?.width?.div(2f)
        val cy = mBitmap?.height?.div(2f)
        val flippedBitmap = mBitmap?.flip(-1f, 1f, cx!!, cy!!)
        photoEditorView?.source?.setImageBitmap(flippedBitmap)
        mBitmap = flippedBitmap
    }

    private fun rotateImg(angle : Int) {
        sbAngle?.visibility = View.GONE
        sbZoom?.visibility = View.GONE

        val alteredBitmap = mBitmap?.let { rotateImage(it, angle) }
        photoEditorView?.source?.setImageBitmap(alteredBitmap)
        mBitmap = alteredBitmap
    }

    private fun showAdjustView() {
        rlBack?.visibility = View.GONE
        bottomNavView?.visibility = View.GONE
        filterView?.visibility = View.GONE
        adjustView?.visibility = View.VISIBLE

        llViews?.visibility = View.VISIBLE

        tvAdjust?.setTextAppearance(this, R.style.TextView_xl_selected)
        tvFilter?.setTextAppearance(this, R.style.TextView_xl)

        val adjustLayoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        rvAdjust?.layoutManager = adjustLayoutManager
        rvAdjust?.setHasFixedSize(true)
        adjustAdapter = AdjustAdapter(Utils.getAdjustDataSet(this))
        rvAdjust?.adapter = adjustAdapter

    }

    private fun showFilterView() {
        rlBack?.visibility = View.GONE
        bottomNavView?.visibility = View.GONE
        adjustView?.visibility = View.GONE
        filterView?.visibility = View.VISIBLE

        llViews?.visibility = View.VISIBLE

        tvFilter?.setTextAppearance(this, R.style.TextView_xl_selected)
        tvAdjust?.setTextAppearance(this, R.style.TextView_xl)

       /* val filterLayoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        rvFilter?.layoutManager = filterLayoutManager
        rvFilter?.setHasFixedSize(true)
        rvFilter?.adapter = FilterAdapter(filterData)*/
    }

    override fun onFailure(e: Exception?) {
        Log.d("TAG", e.toString())
    }

    override fun onBitmapReady(saveBitmap: Bitmap?) {
        mBitmap = saveBitmap
        photoEditorView?.source?.setImageBitmap(mBitmap)
    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getInstance().unregister(this)
    }

    override fun onTouch(view: View?, event: MotionEvent?): Boolean {
        when(event?.action){
            MotionEvent.ACTION_DOWN -> removeColorFilter()
            MotionEvent.ACTION_UP -> applyFilters(mBitmap, contrastValue, lightnessValue, saturationValue, temperatureValue)
        }
        return true
    }

    override fun onClick(view: View?) {
        when(view?.id){
            R.id.tv_adjust -> showAdjustView()
            R.id.tv_filter -> showFilterView()
            R.id.iv_save, R.id.ivsave, R.id.save -> saveImage(false)
            R.id.iv_close -> discardChanges()
            R.id.iv_back -> onBackPressed()
            R.id.tv_save -> saveImage(true)
        }
    }

    private fun discardChanges() {
        finish()
    }

    @SuppressLint("MissingPermission")
    private fun saveImage(isFinalSave : Boolean) {
        if (requestPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            val path = Environment.getExternalStorageDirectory().toString() + File.separator + "" + System.currentTimeMillis() + ".png"
            val file = File(path)

            try {
                file.createNewFile()
                val saveSettings = SaveSettings.Builder()
                        .setClearViewsEnabled(true)
                        .setTransparencyEnabled(true)
                        .build()

                mPhotoEditor?.saveAsFile(path, saveSettings, object : PhotoEditor.OnSaveListener {
                    override fun onSuccess(imagePath: String) {
                        if(isFinalSave) {
                            val intent = Intent(applicationContext, SaveAndShareActivity::class.java)
                            intent.putExtra("path", imagePath)
                            SEND_PATH_OTHER_APP = imagePath
                            startActivityForResult(intent, FINAL_SAVING_REQUEST)
                        } else {
                            mEditViewModel?.saveImage(path)
                            rlBack?.visibility = View.VISIBLE
                            bottomNavView?.visibility = View.VISIBLE
                            adjustView?.visibility = View.GONE
                            filterView?.visibility = View.GONE
                            backgroundView?.visibility = View.GONE
                            rotateView?.visibility = View.GONE

                            llViews?.visibility = View.GONE
                        }
                    }

                    override fun onFailure(exception: java.lang.Exception) {
                        finish()
                    }
                })

            } catch (e: Exception) {

            }
        } else {

        }
        llSave?.visibility = View.GONE
    }

    override fun onProgressChanged(seekbar: SeekBar?, progress: Int, bool: Boolean) {
        when(typeId) {
            Constants.AdjustItems.LIGHTNESS.ID -> lightnessValue = progress.toFloat() - 100f
            Constants.AdjustItems.CONTRAST.ID -> contrastValue = progress.toFloat()/100f
            Constants.AdjustItems.WRAMTH.ID -> temperatureValue = progress.toFloat()/700f
            Constants.AdjustItems.SATURATION.ID -> saturationValue = progress.toFloat()/100f
            Constants.RotateItems.ANGEL.ID -> photoEditorView?.rotation = progress.toFloat() - 100f
            Constants.RotateItems.ZOOM.ID -> zoomImage(progress.toFloat()/100f)
        }
    }

    private fun zoomImage(factor: Float) {
        photoEditorView?.scaleX = factor
        photoEditorView?.scaleY = factor
    }

    override fun onStartTrackingTouch(seekbar: SeekBar?) {}

    override fun onStopTrackingTouch(seekbar: SeekBar?) {
        applyFilters(mBitmap, contrastValue, lightnessValue, saturationValue, temperatureValue)
    }

    fun applyFilters(bmp: Bitmap?, contrast: Float, brightness: Float, saturation: Float, temperature: Float) {
        contrastValue = contrast
        lightnessValue = brightness
        saturationValue = saturation
        temperatureValue = temperature
        val cm = ColorMatrix(floatArrayOf(
                contrastValue, 0f, 0f, 0f, lightnessValue,
                0f, contrastValue, 0f, 0f, lightnessValue,
                0f, 0f, contrastValue, 0f, lightnessValue,
                0f, 0f, 0f, 1f, 0f))

        val ret = bmp?.width?.let { Bitmap.createBitmap(it, bmp.height, bmp.config) }
        val canvas = ret?.let { Canvas(it) }
        val paint = Paint()
        paint.colorFilter = ColorMatrixColorFilter(cm)
        canvas?.drawBitmap(bmp, 0f, 0f, paint)
        changedBitmap = ret?.let { Utils.changeSaturation(it, saturationValue) }?.let { Utils.changeTemperature(it, temperatureValue) }
        photoEditorView?.source?.setImageBitmap(changedBitmap)

    }

    private fun removeColorFilter() {
        val cm = ColorMatrix(floatArrayOf(
                1f, 0f, 0f, 0f, 1f,
                0f, 1f, 0f, 0f, 1f,
                0f, 0f, 1f, 0f, 1f,
                0f, 0f, 0f, 1f, 0f))

        val ret = mBitmap?.width?.let { Bitmap.createBitmap(it, mBitmap!!.height, mBitmap!!.config) }
        val canvas = ret?.let { Canvas(it) }
        val paint = Paint()
        paint.colorFilter = ColorMatrixColorFilter(cm)
        mBitmap?.let { canvas?.drawBitmap(it, 0f, 0f, paint) }
        val alterBitmap = ret?.let { Utils.changeSaturation(it, 0.9f) }?.let { Utils.changeTemperature(it, 0f) }
        photoEditorView?.source?.setImageBitmap(alterBitmap)
    }

    override fun OnClickBackground(color: Int) {
        blurBackgroundAdapter?.setVisibilityGoneRingShapeView(-1)
        photoEditorView?.setBackgroundColor(color)
    }

    override fun onClickBlurBackGroundItem(bitmap: Bitmap?) {
        if(bitmap != null){
            val d = BitmapDrawable(resources, bitmap)
            photoEditorView?.background = d
        }
    }

    override fun onClickBlurBackGroundItem(pos: Int) {
        if(pos == 0) photoEditorView?.setBackgroundColor(0)
        if(pos == 1) {
            if(blurBackgroundAdapter?.setImagePos == -2){
                blurBackgroundAdapter?.setImage(-1)
                blurBackgroundAdapter?.setBitmap(mBitmap)
                if (blurBackgroundAdapter?.getBitmap() != null) {
                    onClickBlurBackGroundItem(blurBackgroundAdapter?.getBitmap())
                    blurBackgroundAdapter?.getLastTimePos()?.let { blurBackgroundAdapter?.setVisibilityGoneRingShapeView(it) }
                } else run {
                    onClickBlurBackGroundItem(0)
                    blurBackgroundAdapter?.setVisibilityGoneRingShapeView(0)
                }
            } else {
               /* val intent = Intent(this, MainActivity::class.java)
                intent.putExtra(Constants.BACKGROUND_REFERRER, "background")
                startActivityForResult(intent, SELECT_BACKGROUND)*/
            }
        }
    }

    override fun onBackPressed() {
        if(adjustView?.visibility == View.VISIBLE
                || filterView?.visibility == View.VISIBLE
                || rotateView?.visibility == View.VISIBLE
                || backgroundView?.visibility == View.VISIBLE) {

            rlBack?.visibility = View.VISIBLE
            bottomNavView?.visibility = View.VISIBLE
            adjustView?.visibility = View.GONE
            filterView?.visibility = View.GONE
            rotateView?.visibility = View.GONE
            backgroundView?.visibility = View.GONE
            llViews?.visibility = View.GONE

            return
        }
        super.onBackPressed()
    }

    fun requestPermission(permission: String): Boolean {
        val isGranted = ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED
        if (!isGranted) {
            ActivityCompat.requestPermissions(
                    this,
                    arrayOf(permission),
                    READ_WRITE_STORAGE)
        }
        return isGranted
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                CROP_IMAGE -> {
                    if(data != null) {
                        val uri = data.data
                        if(uri != null){
                            imgPath = Utils.getImagePathFromUri(this, uri)
                            loadImage()
                        }
                    }
                }
                BACKGROUND_REQUEST -> {
                    if(data != null) {
                        val colorName = data.getStringExtra(BackgroundColorActivity.NAME)
                        backgroundColorAdapter?.notifyColorList(colorName)

                    }
                }
                SELECT_BACKGROUND -> {
                    if(data != null) {
                        val file = File(data.type)
                        if(file != null && file.exists()) {
                            try{
                                var bitmap : Bitmap? = null
                                GlobalScope.launch {
                                    bitmap = BitmapFactory.decodeFile(file.absolutePath)
                                }
                                blurBackgroundAdapter?.setBitmap(bitmap)
                                blurBackgroundAdapter?.setImage(-2)
                                blurBackgroundAdapter?.setVisibilityGoneRingShapeView(-1)
                            } catch (e : Exception){

                            }
                        }
                    }
                }
                FINAL_SAVING_REQUEST -> {
                    val intent = Intent()
                    intent.putExtra("", SEND_PATH_OTHER_APP)
                    setResult(Activity.RESULT_OK, intent)
                    finish()
                }
            }
        }
    }

    interface PrepareFilterListener {
        fun onFilterPrepared(thumbnailItemList: ArrayList<ThumbnailItem>?)
    }

}

