package com.example.editor.view

import android.os.Bundle
import com.example.editor.R
import com.example.editor.constant.Constants

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        loadFragment()
    }

    private fun loadFragment() {
        var referrerString : String? = null
        if(intent != null && null != intent.getStringExtra(Constants.BACKGROUND_REFERRER)){
            referrerString = intent.getStringExtra(Constants.BACKGROUND_REFERRER)
        }
        val galleryFragment = GalleryFragment.getInstance(referrerString)
        supportFragmentManager.beginTransaction().add(R.id.fragment_container, galleryFragment).commit()
    }
}
