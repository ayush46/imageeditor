package com.example.editor.view

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.editor.R
import com.example.editor.adapter.BackgroundColorAdapter
import kotlinx.android.synthetic.main.activity_bakground_color.*

class BackgroundColorActivity : BaseActivity() {
    companion object {
        const val NAME = "name"
        private const val PREF_NAME = "perf"
        private const val CLASSIC = "classic"
        private const val MODERN = "modern"
        private const val MORDANI = "morandi"
        private const val Key = "key"
    }

    private var mModernAdapter: BackgroundColorAdapter? = null
    private var mClassicAdapter: BackgroundColorAdapter? = null
    private var mMorandniAdapter: BackgroundColorAdapter? = null
    private var sharedPreferences: SharedPreferences? = null


    private var setColorListName: String = MODERN
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bakground_color)
        sharedPreferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        setColorListName = sharedPreferences!!.getString(Key, MODERN).toString()
        setImageResource(setColorListName)
        setAdapter()
        setListener()
    }

    private fun setImageResource(colorListName: String) {
        when (colorListName) {
            MORDANI -> {
                morandi_check.setImageResource(R.drawable.ic_tick)
                classic_check.setImageResource(R.drawable.ic_radio_button_unchecked_white_24dp)
                modern_check.setImageResource(R.drawable.ic_radio_button_unchecked_white_24dp)
            }
            CLASSIC -> {
                classic_check.setImageResource(R.drawable.ic_tick)
                modern_check.setImageResource(R.drawable.ic_radio_button_unchecked_white_24dp)
                morandi_check.setImageResource(R.drawable.ic_radio_button_unchecked_white_24dp)
            }
            MODERN -> {
                modern_check.setImageResource(R.drawable.ic_tick)
                classic_check.setImageResource(R.drawable.ic_radio_button_unchecked_white_24dp)
                morandi_check.setImageResource(R.drawable.ic_radio_button_unchecked_white_24dp)
            }
        }

    }

    private fun setAdapter() {
        mModernAdapter = BackgroundColorAdapter(this@BackgroundColorActivity)
        mClassicAdapter = BackgroundColorAdapter(this@BackgroundColorActivity)
        mMorandniAdapter = BackgroundColorAdapter(this@BackgroundColorActivity)

        with(modern_color_list) {
            layoutManager = LinearLayoutManager(baseContext, LinearLayoutManager.HORIZONTAL, false)
            adapter = mModernAdapter
            mModernAdapter!!.modernList()
            mModernAdapter!!.setVisibilityFlag()
        }
        with(classic) {
            layoutManager = LinearLayoutManager(baseContext, LinearLayoutManager.HORIZONTAL, false)
            adapter = mClassicAdapter
            mClassicAdapter!!.classicList()
            mClassicAdapter!!.setVisibilityFlag()
        }
        with(morandi) {
            layoutManager = LinearLayoutManager(baseContext, LinearLayoutManager.HORIZONTAL, false)
            adapter = mMorandniAdapter
            mMorandniAdapter!!.morandniList()
            mMorandniAdapter!!.setVisibilityFlag()
        }
    }

    private fun setListener() {
        classic_check.setOnClickListener {
            classic_check.setImageResource(R.drawable.ic_check_circle_white_24dp)
            morandi_check.setImageResource(R.drawable.ic_radio_button_unchecked_white_24dp)
            modern_check.setImageResource(R.drawable.ic_radio_button_unchecked_white_24dp)
            setColorListName = CLASSIC
        }
        modern_check.setOnClickListener {
            modern_check.setImageResource(R.drawable.ic_check_circle_white_24dp)
            morandi_check.setImageResource(R.drawable.ic_radio_button_unchecked_white_24dp)
            classic_check.setImageResource(R.drawable.ic_radio_button_unchecked_white_24dp)
            setColorListName = MODERN
        }

        morandi_check.setOnClickListener {
            morandi_check.setImageResource(R.drawable.ic_check_circle_white_24dp)
            classic_check.setImageResource(R.drawable.ic_radio_button_unchecked_white_24dp)
            modern_check.setImageResource(R.drawable.ic_radio_button_unchecked_white_24dp)
            setColorListName = MORDANI

        }

        done.setOnClickListener {
            val editor: SharedPreferences.Editor = sharedPreferences!!.edit()
            editor.putString(Key, setColorListName)
            editor.apply()
            editor.commit()
            setResult(RESULT_OK, intent.putExtra(NAME, setColorListName))
            finish()
        }
    }
}
