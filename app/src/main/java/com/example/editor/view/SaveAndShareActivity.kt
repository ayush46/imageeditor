package com.example.editor.view

import android.app.Activity
import android.app.Dialog
import android.content.ComponentName
import android.content.Intent
import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Toast
import android.widget.Toast.LENGTH_LONG
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.editor.R
import com.example.editor.adapter.ShareButtonAdapter
import com.example.editor.constant.Constants
import kotlinx.android.synthetic.main.activity_save_share.*
import kotlinx.android.synthetic.main.show_app_installed_dilaog.*
import java.io.File
import java.util.*

class SaveAndShareActivity : AppCompatActivity(), ShareButtonAdapter.OnClickShareButton{

    private var imagePath: String? = null
    private var imageUri: Uri? = null
    private var mAdItems: ArrayList<Any>? =null
    /*private var mNativeAdEnabled = false
    private var mediaEnabled = false
    private var musicAdEnabled = false*/

    companion object{
        const val MESSENGER_PACKAGE="com.facebook.orca"
        const val TWITTER_PACKAGE="com.twitter.android"
        const val INSTA_PACKAGE="com.instagram.android"
        const val WHATSAPP_PACKAGE="com.whatsapp"
        const val FACEBOOK_PACKAGE="com.facebook.katana"
        const val MUSIC_APP_PACKAGE="com.rocks.music"
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_save_share)
        mAdItems = ArrayList()
        with(share_Button_rv) {
            layoutManager = LinearLayoutManager(baseContext, LinearLayoutManager.HORIZONTAL, false)
            adapter = ShareButtonAdapter(baseContext, this@SaveAndShareActivity)

        }
        //  fbNativeAdenable = true   musicAdEnabled=true
       /* musicAdEnabled = RemotConfigUtils.getMusicNativeAdsEnableValue(this)
        if (musicAdEnabled && !isAppInstalled(MUSIC_APP_PACKAGE)){
            loadNativeMusicAd()
        }else {
            mNativeAdEnabled = RemotConfigUtils.getGoogleNativeAdsEnableValue(baseContext)
            if (mNativeAdEnabled && BuildConfig.ADD_MOB && isNetworkConnected()) {
                lodNativeAd()
            }
        }*/
        loadImage()
        setListener()
    }

    /*private fun loadNativeMusicAd() {
        music_native_ad.visibility=View.VISIBLE
    }*/

   /* private fun setNativeAdId(addLoaded: Boolean) {
        ad_view.bodyView = native_ad_body
        ad_view.advertiserView = native_ad_sponsored_label
        ad_view.callToActionView = native_ad_call_to_action
        if(addLoaded){
            if(mAdItems?.size!! >0) {
                ad = mAdItems?.get(0) as UnifiedNativeAd?
            }
            if ( ad!=null) {
                native_ads.visibility = View.VISIBLE
                native_ad_body.text = ad?.body
               *//* var textPriceStore = ""
                if (!TextUtils.isEmpty(ad?.store)) {
                    textPriceStore = "" + ad?.store
                }
                if (!TextUtils.isEmpty(ad?.price)) {
                    textPriceStore = " " + ad?.price
                }
                native_ad_social_context.text = "$textPriceStore"*//*
                native_ad_social_context.text = ad?.advertiser
                native_ad_call_to_action.text = ad?.callToAction
                ad_view.storeView = native_ad_social_context
                ad_view.mediaView = native_ad_media
                ad_view.iconView = native_ad_icon
                native_ad_title.text = ad?.headline

                if (ad?.icon != null) {
                    (ad_view.iconView as ImageView).setImageDrawable(ad?.icon?.drawable)
                    ad_view.iconView.visibility = View.VISIBLE
                }else{
                    ad_view.iconView.visibility = View.GONE
                }

                ad_view.setNativeAd(ad)

            }
            else{
                native_ads.visibility = View.GONE

            }
        }else {
            native_ads.visibility=View.GONE
        }

    }

    private fun lodNativeAd() {
        try {
            val builder = AdLoader.Builder(baseContext, baseContext.getString(com.rocks.themelibrary.R.string.native_ad_unit_id))
            // builder.withNativeAdOptions(adOptions);
            val adLoader = builder.forUnifiedNativeAd { unifiedNativeAd ->
                // A native ad loaded successfully, check if the ad loader has finished loading
                // and if so, insert the ads into the list.
                // A native ad loaded successfully, check if the ad loader has finished loading
                // and if so, insert the ads into the list.
                mAdItems?.add(unifiedNativeAd)
                setNativeAdId(true)

            }.withAdListener(
                    object : AdListener() {
                        override fun onAdFailedToLoad(errorCode: Int) { // A native ad failed to load, check if the ad loader has finished loading
                            // and if so, insert the ads into the list.
                            Log.e("MainActivity", "The previous native ad failed to load. Attempting to"+ " load another.")
                            setNativeAdId(false)

                        }
                    }).build()
            // Load the Native ads.
            val nativeAdOptions=NativeAdOptions.Builder().setAdChoicesPlacement(NativeAdOptions.ADCHOICES_TOP_RIGHT).build()
            builder.withNativeAdOptions(nativeAdOptions)
            adLoader.loadAds(AdRequest.Builder().build(), 1)
        } catch (e: Exception) {
        }
    }*/

    private fun setListener() {
        with(back) {
            setOnClickListener {
                finish()
            }

            with(home) {
                setOnClickListener {
                    setResult(Activity.RESULT_OK)
                    finish()
                }
            }
        }

        image_view_layout.setOnClickListener {
            button_layout.visibility = View.GONE
            full_image_layout.visibility = View.VISIBLE
            Handler().postDelayed({
                full_image_view.setImageURI(imageUri)
                full_image_progressbar.visibility = View.GONE
//                full_image_layout.animation=AnimationUtils.loadAnimation(baseContext,R.anim.fade_in)

            }, 500)
        }
         full_image_view.setOnClickListener { view ->
             onBackPressed()
         }
        /*btn_install.setOnClickListener {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=$MUSIC_APP_PACKAGE")))
        }*/

    }

    private fun loadImage() {

        imagePath = intent.getStringExtra("path")
        imageUri = Uri.fromFile(File(imagePath))
        show_edit_image.setImageURI(imageUri)
        progress_bar.visibility = View.VISIBLE
        Handler().postDelayed({
            progress_bar.visibility = View.GONE
            show_edit_image.visibility = View.VISIBLE
            search.visibility = View.VISIBLE
        }, 500)

    }

    private fun shareImageOther() {
        val shareIntent = Intent(Intent.ACTION_SEND)
        shareIntent.type = "image/*"
        val file = File(imagePath)
        val uri = FileProvider.getUriForFile(applicationContext, Constants.FILE_PROVIDER, file)
        shareIntent.putExtra(Intent.EXTRA_STREAM, uri)
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        startActivity(shareIntent);
    }

    override fun onClickShareButton(buttonName: ShareButtonAdapter.ButtonType) = when (buttonName) {
        ShareButtonAdapter.ButtonType.SAVE -> Toast.makeText(this, "" + imagePath, LENGTH_LONG).show()
        ShareButtonAdapter.ButtonType.OTHER -> shareImageOther()
        ShareButtonAdapter.ButtonType.E_MAIL -> shareEmail()
        ShareButtonAdapter.ButtonType.WHATSAPP -> shareImage(WHATSAPP_PACKAGE,buttonName.name)
        ShareButtonAdapter.ButtonType.FACEBOOK -> shareImage(FACEBOOK_PACKAGE,buttonName.name)
        ShareButtonAdapter.ButtonType.INSTAGRAM -> shareImage(INSTA_PACKAGE,buttonName.name)
        ShareButtonAdapter.ButtonType.MESSENGER -> shareImage(MESSENGER_PACKAGE,buttonName.name)
        ShareButtonAdapter.ButtonType.TWITTER -> shareImage(TWITTER_PACKAGE,buttonName.name)
    }

    private fun shareImage(packages: String,notInstalledAppName: String) {
        if(isAppInstalled(packages)) {
            val file = File(imagePath)
            val uri = FileProvider.getUriForFile(applicationContext, Constants.FILE_PROVIDER, file)
            val sendIntent = Intent(Intent.ACTION_VIEW);
            sendIntent.type = "image/*"
            sendIntent.action = Intent.ACTION_SEND;
            sendIntent.putExtra(Intent.EXTRA_STREAM, uri);
            sendIntent.setPackage(packages);
            sendIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            try {
                startActivity(Intent.createChooser(sendIntent, "Share images..."));
            } catch (ex: android.content.ActivityNotFoundException) {
                ex.printStackTrace()
            }
        }else{
            showDialogForInstalledApp(notInstalledAppName,packages)
        }

    }

    private fun showDialogForInstalledApp(appName:String,packages: String) {
        val dialog = Dialog(this).also {
            it.setContentView(R.layout.show_app_installed_dilaog)
        }
        dialog.tv_1.text= "${appName[0]+appName.substring(1).toLowerCase()} is not Installed"
        dialog.tv_2.text="Download ${appName[0]+appName.substring(1).toLowerCase()} or share with other\napps"
        dialog.tv_3.text="DOWNLOAD ${appName.toUpperCase()}"
        dialog.tv_3.setOnClickListener {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=$packages")))
        }
        dialog.tv_4.setOnClickListener {
            shareImageOther()
        }
        dialog.tv_5.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }


    private fun isAppInstalled(packages: String): Boolean {
        return try {
            applicationContext.packageManager.getApplicationInfo(packages, 0)
            true
        } catch (e: PackageManager.NameNotFoundException) {
            false
        }
    }

    private fun shareEmail() {
        val file = File(imagePath)
        val uri = FileProvider.getUriForFile(applicationContext, Constants.FILE_PROVIDER, file)
        val intent = Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", "", null))
        intent.putExtra(Intent.EXTRA_SUBJECT, "")
        intent.putExtra(Intent.EXTRA_TEXT, "")
        intent.putExtra(Intent.EXTRA_STREAM, uri)
        val resolveInfos: List<ResolveInfo> = packageManager.queryIntentActivities(intent, 0)
        if (resolveInfos.isEmpty()) {
            Toast.makeText(baseContext,"please install email ",Toast.LENGTH_SHORT).show()
        } else {
            val packageName = resolveInfos[0].activityInfo.packageName
            val name = resolveInfos[0].activityInfo.name
            intent.action = Intent.ACTION_SEND
            intent.component = ComponentName(packageName, name)
            startActivity(intent)
        }
    }

    override fun onBackPressed() {
        if (full_image_layout.visibility == View.VISIBLE) {
//            full_image_layout.animation=AnimationUtils.loadAnimation(baseContext,R.anim.fade_out)
            full_image_layout.visibility = View.GONE
            button_layout.visibility = View.VISIBLE
        } else {
            super.onBackPressed()
        }
    }
    /*private fun isNetworkConnected():Boolean{
        val connectivityManager = this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }*/

    override fun onDestroy() {
//        ad?.destroy()
        super.onDestroy()
    }


}

