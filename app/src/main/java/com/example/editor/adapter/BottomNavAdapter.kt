package com.example.editor.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.editor.R
import com.example.editor.model.BottomNavModel
import com.example.editor.viewholder.BottomNavViewHolder

class BottomNavAdapter(list : ArrayList<BottomNavModel>) : RecyclerView.Adapter<BottomNavViewHolder>(){

    private var dataList : ArrayList<BottomNavModel> = list

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BottomNavViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val mView = inflater.inflate(R.layout.bottom_item_view, parent, false)
        return BottomNavViewHolder(mView)
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onBindViewHolder(holder: BottomNavViewHolder, position: Int) {
        val bottomNavModel = dataList.get(position)
        bottomNavModel.let { holder.bindViewData(it.imageDrawable, it.title, it.type) }
    }

}