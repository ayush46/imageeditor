package com.example.editor.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.editor.R
import com.example.editor.model.CropModel
import com.example.editor.viewholder.CropViewHolder
import com.isseiaoki.simplecropview.CropImageView

class CropAdapter(list : ArrayList<CropModel>) : RecyclerView.Adapter<CropViewHolder>() {

    private var cropModelList : ArrayList<CropModel>? = null
    private var selectedCropMode = CropImageView.CropMode.FREE

    init {
        cropModelList = list
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CropViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val mView = inflater.inflate(R.layout.crop_item_view, parent, false)
        return CropViewHolder(mView)
    }

    override fun getItemCount(): Int {
        return cropModelList?.size!!
    }

    override fun onBindViewHolder(holder: CropViewHolder, position: Int) {
        val cropModel = cropModelList?.get(position)
        holder.bindData(cropModel, selectedCropMode)
    }

    fun setCropMode(cropMode: CropImageView.CropMode?) {
        selectedCropMode = cropMode!!
        notifyDataSetChanged()
    }
}