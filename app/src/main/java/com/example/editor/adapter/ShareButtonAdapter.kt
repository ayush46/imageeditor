package com.example.editor.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.editor.R
import com.example.editor.viewholder.ShareButtonHolder
import kotlinx.android.synthetic.main.share_button_row.view.*

class ShareButtonAdapter(context: Context,onClickShareButton: OnClickShareButton) : RecyclerView.Adapter<ShareButtonHolder>() {
   private var buttonList=ArrayList<ButtonData>()
   private var onClickShareButton: OnClickShareButton? =null
   private  var context: Context? =null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShareButtonHolder {

        return ShareButtonHolder(LayoutInflater.from(parent.context).inflate(R.layout.share_button_row,parent,false),buttonList,onClickShareButton)
    }

    override fun getItemCount(): Int {
        return buttonList.size
    }

    override fun onBindViewHolder(holder: ShareButtonHolder, position: Int) {
                  holder.itemView.image.setImageResource(buttonList[position].drawable)

        when (position) {
            4 -> {
                holder.itemView.text.text = "" +buttonList[position].buttonName.name.substring(0,1).toUpperCase()+
                        ""+buttonList[position].buttonName.name.substring(1,buttonList[position].buttonName.name.length).replace("_","-").toLowerCase()
            }
            else -> {
                holder.itemView.text.text = "" +buttonList[position].buttonName.name.substring(0,1).toUpperCase()+
                        ""+buttonList[position].buttonName.name.substring(1,buttonList[position].buttonName.name.length).toLowerCase()
            }
        }
    }



    data class ButtonData(val drawable: Int, val buttonName: ButtonType)
  enum class ButtonType {
      SAVE, OTHER, WHATSAPP,FACEBOOK,E_MAIL,INSTAGRAM,MESSENGER,TWITTER;
  }

    init {
        this.onClickShareButton=onClickShareButton
        this.context=context
        buttonList()
    }

    private fun buttonList() {
        buttonList.add(ButtonData(R.drawable.transperent_save, ButtonType.SAVE))
        buttonList.add(ButtonData(R.drawable.transperent_share, ButtonType.OTHER))
        buttonList.add(ButtonData(R.drawable.whatsapp, ButtonType.WHATSAPP))
        buttonList.add(ButtonData(R.drawable.fb, ButtonType.FACEBOOK))
        buttonList.add(ButtonData(R.drawable.mail, ButtonType.E_MAIL))
        buttonList.add(ButtonData(R.drawable.insta, ButtonType.INSTAGRAM))
        buttonList.add(ButtonData(R.drawable.messenger, ButtonType.MESSENGER))
        buttonList.add(ButtonData(R.drawable.twitter, ButtonType.TWITTER))
    }

    interface OnClickShareButton{
        fun onClickShareButton(buttonName: ButtonType):Any?
    }
}