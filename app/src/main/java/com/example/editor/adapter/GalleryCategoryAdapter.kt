package com.example.editor.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.editor.R
import com.example.editor.viewholder.GalleryCategoryViewHolder

class GalleryCategoryAdapter(imageCategoryMap : HashMap<String,ArrayList<String>>?) : RecyclerView.Adapter<GalleryCategoryViewHolder>() {

    private val categories : HashMap<String,ArrayList<String>>? = imageCategoryMap

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GalleryCategoryViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val mView = inflater.inflate(R.layout.gallery_category_item, parent, false)
        return GalleryCategoryViewHolder(mView)
    }

    override fun getItemCount(): Int {
        return categories?.size!!
    }

    override fun onBindViewHolder(holder: GalleryCategoryViewHolder, position: Int) {
        categories?.let {
            val tempList = ArrayList<String>()
            tempList.addAll(it.keys)
            val key = tempList.get(position)
            holder.bindData(key, categories[key])
        }
    }

}