package com.example.editor.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.editor.R
import com.example.editor.viewholder.GalleryViewHolder
import java.util.*

class GalleryAdapter(imageList : ArrayList<String>?) : RecyclerView.Adapter<GalleryViewHolder>() {

    private val images : ArrayList<String>? = imageList

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GalleryViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val mView = inflater.inflate(R.layout.gallery_list_item, parent, false)
        return GalleryViewHolder(mView)
    }

    override fun getItemCount(): Int {
        return images?.size!!
    }

    override fun onBindViewHolder(holder: GalleryViewHolder, position: Int) {
        images?.get(position)?.let { holder.bindViewData(it) }
    }

    fun updateData(imageList: ArrayList<String>?) {
        if(!images?.isNullOrEmpty()!!){
            images.clear()
            imageList?.let { images.addAll(it) }
        }
        notifyDataSetChanged()
    }

}