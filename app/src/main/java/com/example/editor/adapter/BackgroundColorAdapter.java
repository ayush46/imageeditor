package com.example.editor.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.editor.R;
import com.example.editor.view.BackgroundColorActivity;
import com.example.editor.view.EditActivity;

import java.util.ArrayList;

public class BackgroundColorAdapter extends RecyclerView.Adapter<BackgroundColorAdapter.BackgroundHolder> {
    private ArrayList<Integer> colors = new ArrayList<>();
    private Context context;
    private View mView;
    private boolean seVisibilityFlag = false;
    private ClickOnBackgroundItem clickOnBackgroundItem;

    public BackgroundColorAdapter(Context context, ClickOnBackgroundItem clickOnBackgroundItem) {
        this.clickOnBackgroundItem = clickOnBackgroundItem;
        this.context = context;
    }

    public BackgroundColorAdapter(Context context) {
        this.context = context;
    }

    public void notifyColorList(String name) {
        if (name.equalsIgnoreCase("classic")) {
            classicList();
        } else if (name.equalsIgnoreCase("modern")) {
            modernList();
        } else if (name.equalsIgnoreCase("morandi")) {
            morandniList();
        } else {
            modernList();
        }
        notifyDataSetChanged();

    }

    public void setVisibilityFlag() {
        seVisibilityFlag = true;
    }

    public void classicList() {
        colors = classic();
    }

    public void modernList() {
        colors = modern();
    }

    public void morandniList() {
        colors = morandni();
    }

    @NonNull
    @Override
    public BackgroundHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_background, parent, false);
        return new BackgroundHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull BackgroundHolder holder, int position) {
        holder.colorPicker.setBackgroundColor(colors.get(position));
        if (position == colors.size() - 1) {
            holder.colorPicker.setImageResource(R.drawable.ic_add_circle_white_24dp);
            if (seVisibilityFlag) {
                holder.colorPicker.setVisibility(View.GONE);
            } else {
                setMargins(holder.colorPicker, 20, 0, 10, 0);
            }

        } else {
            holder.colorPicker.setImageResource(0);
            if (seVisibilityFlag) {
                holder.colorPicker.setVisibility(View.VISIBLE);
            } else {
                setMargins(holder.colorPicker, 0, 0, 0, 0);
            }
        }
    }

    private void setMargins(View view, int left, int top, int right, int bottom) {
        if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            p.setMargins(left, top, right, bottom);
            view.requestLayout();
        }
    }

    @Override
    public int getItemCount() {
        return colors.size();
    }

    public class BackgroundHolder extends RecyclerView.ViewHolder {
        private ImageView colorPicker;

        public BackgroundHolder(@NonNull View itemView) {
            super(itemView);
            colorPicker = itemView.findViewById(R.id.color_picker);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (clickOnBackgroundItem != null && getAdapterPosition() > -1 && getAdapterPosition() < colors.size() - 1) {
                        clickOnBackgroundItem.OnClickBackground(colors.get(getAdapterPosition()));
                    }
                    if (clickOnBackgroundItem != null && getAdapterPosition() == colors.size() - 1 && context != null) {
                        ((Activity) context).startActivityForResult(new Intent(context, BackgroundColorActivity.class), EditActivity.Companion.getBACKGROUND_REQUEST());
                    }
                }
            });
        }
    }

    public interface ClickOnBackgroundItem {
        void OnClickBackground(int color);
    }

    private ArrayList<Integer> modern() {
        ArrayList<Integer> colors = new ArrayList<>();
        colors.add(ContextCompat.getColor(context, R.color.bg_gray_1));
        colors.add(ContextCompat.getColor(context, R.color.bg_gray_2));
        colors.add(ContextCompat.getColor(context, R.color.bg_gray_3));
        colors.add(ContextCompat.getColor(context, R.color.bg_gray_4));
        colors.add(ContextCompat.getColor(context, R.color.bg_black));
        colors.add(ContextCompat.getColor(context, R.color.bg_plate_chestnut));
        colors.add(ContextCompat.getColor(context, R.color.bg_salmon));
        colors.add(ContextCompat.getColor(context, R.color.bg_vermilion));
        colors.add(ContextCompat.getColor(context, R.color.bg_red_1));
        colors.add(ContextCompat.getColor(context, R.color.bg_red_2));
        colors.add(ContextCompat.getColor(context, R.color.bg_yellow_1));
        colors.add(ContextCompat.getColor(context, R.color.bg_yellow_2));
        colors.add(ContextCompat.getColor(context, R.color.bg_yellow_3));
        colors.add(ContextCompat.getColor(context, R.color.orange_1));
        colors.add(ContextCompat.getColor(context, R.color.titan));
        colors.add(ContextCompat.getColor(context, R.color.flax));
        colors.add(ContextCompat.getColor(context, R.color.lemon));
        colors.add(ContextCompat.getColor(context, R.color.selective_yellow));
        colors.add(ContextCompat.getColor(context, R.color.pumpkin));
        colors.add(ContextCompat.getColor(context, R.color.pink_1));
        colors.add(ContextCompat.getColor(context, R.color.pink_2));
        colors.add(ContextCompat.getColor(context, R.color.pink_3));
        colors.add(ContextCompat.getColor(context, R.color.pink_4));
        colors.add(ContextCompat.getColor(context, R.color.pink_5));
        colors.add(ContextCompat.getColor(context, R.color.violet_1));
        colors.add(ContextCompat.getColor(context, R.color.violet_2));
        colors.add(ContextCompat.getColor(context, R.color.violet_3));
        colors.add(ContextCompat.getColor(context, R.color.violet_4));
        colors.add(ContextCompat.getColor(context, R.color.violet_5));
        colors.add(ContextCompat.getColor(context, R.color.blue_1));
        colors.add(ContextCompat.getColor(context, R.color.blue_2));
        colors.add(ContextCompat.getColor(context, R.color.blue_3));
        colors.add(ContextCompat.getColor(context, R.color.blue_4));
        colors.add(ContextCompat.getColor(context, R.color.blue_5));
        colors.add(ContextCompat.getColor(context, R.color.blue_6));
        colors.add(ContextCompat.getColor(context, R.color.blue_7));
        colors.add(ContextCompat.getColor(context, R.color.blue_8));
        colors.add(ContextCompat.getColor(context, R.color.blue_9));
        colors.add(ContextCompat.getColor(context, R.color.aqua_1));
        colors.add(ContextCompat.getColor(context, R.color.aqua_2));
        colors.add(ContextCompat.getColor(context, R.color.aqua_3));
        colors.add(ContextCompat.getColor(context, R.color.aqua_5));
        colors.add(ContextCompat.getColor(context, R.color.aqua_6));
        colors.add(ContextCompat.getColor(context, R.color.aqua_7));
        colors.add(ContextCompat.getColor(context, R.color.aqua_8));
        colors.add(ContextCompat.getColor(context, R.color.aqua_9));
        colors.add(ContextCompat.getColor(context, R.color.aqua_10));
        colors.add(ContextCompat.getColor(context, R.color.green_1));
        colors.add(ContextCompat.getColor(context, R.color.green_2));
        colors.add(ContextCompat.getColor(context, R.color.green_3));
        colors.add(ContextCompat.getColor(context, R.color.green_4));
        colors.add(ContextCompat.getColor(context, R.color.green_5));
        colors.add(ContextCompat.getColor(context, R.color.green_7));
        colors.add(ContextCompat.getColor(context, R.color.green_9));
        colors.add(ContextCompat.getColor(context, R.color.green_9));
        colors.add(ContextCompat.getColor(context, R.color.green_10));
        colors.add(ContextCompat.getColor(context, R.color.green_11));
        colors.add(ContextCompat.getColor(context, R.color.green_1));
        colors.add(ContextCompat.getColor(context, R.color.silver_1));
        colors.add(ContextCompat.getColor(context, R.color.brown_1));
        colors.add(ContextCompat.getColor(context, R.color.brown_2));
        colors.add(ContextCompat.getColor(context, R.color.brown_3));
        colors.add(ContextCompat.getColor(context, R.color.brown_4));
        colors.add(ContextCompat.getColor(context, R.color.edit_bototm_bg));
        return colors;
    }

    private ArrayList<Integer> classic() {
        ArrayList<Integer> colors = new ArrayList<>();
        // colors.add(ContextCompat.getColor(context, R.color.bg_gray_1));
        colors.add(ContextCompat.getColor(context, R.color.bg_gray_2));
        colors.add(ContextCompat.getColor(context, R.color.bg_gray_3));
        colors.add(ContextCompat.getColor(context, R.color.bg_gray_4));
        colors.add(ContextCompat.getColor(context, R.color.bg_black));
        colors.add(ContextCompat.getColor(context, R.color.bg_plate_chestnut));
        //  colors.add(ContextCompat.getColor(context, R.color.bg_salmon));
        colors.add(ContextCompat.getColor(context, R.color.bg_vermilion));
        colors.add(ContextCompat.getColor(context, R.color.bg_red_1));
        //colors.add(ContextCompat.getColor(context, R.color.bg_red_2));
        colors.add(ContextCompat.getColor(context, R.color.bg_yellow_1));
        colors.add(ContextCompat.getColor(context, R.color.bg_yellow_2));
        colors.add(ContextCompat.getColor(context, R.color.bg_yellow_3));
        colors.add(ContextCompat.getColor(context, R.color.orange_1));
        //  colors.add(ContextCompat.getColor(context, R.color.titan));
        colors.add(ContextCompat.getColor(context, R.color.flax));
        colors.add(ContextCompat.getColor(context, R.color.lemon));
        colors.add(ContextCompat.getColor(context, R.color.selective_yellow));
        //  colors.add(ContextCompat.getColor(context, R.color.pumpkin));
        colors.add(ContextCompat.getColor(context, R.color.pink_1));
        //  colors.add(ContextCompat.getColor(context, R.color.pink_2));
        colors.add(ContextCompat.getColor(context, R.color.pink_3));
        colors.add(ContextCompat.getColor(context, R.color.pink_4));
        colors.add(ContextCompat.getColor(context, R.color.pink_5));
        colors.add(ContextCompat.getColor(context, R.color.violet_1));
        //   colors.add(ContextCompat.getColor(context, R.color.violet_2));
        colors.add(ContextCompat.getColor(context, R.color.violet_3));
        colors.add(ContextCompat.getColor(context, R.color.violet_4));
        colors.add(ContextCompat.getColor(context, R.color.violet_5));
        //  colors.add(ContextCompat.getColor(context, R.color.blue_1));
        colors.add(ContextCompat.getColor(context, R.color.blue_2));
        colors.add(ContextCompat.getColor(context, R.color.blue_3));
        //colors.add(ContextCompat.getColor(context, R.color.blue_4));
        colors.add(ContextCompat.getColor(context, R.color.blue_5));
        colors.add(ContextCompat.getColor(context, R.color.blue_6));
        //  colors.add(ContextCompat.getColor(context, R.color.blue_7));
        colors.add(ContextCompat.getColor(context, R.color.blue_8));
        colors.add(ContextCompat.getColor(context, R.color.blue_9));
        // colors.add(ContextCompat.getColor(context, R.color.aqua_1));
        colors.add(ContextCompat.getColor(context, R.color.aqua_2));
        colors.add(ContextCompat.getColor(context, R.color.aqua_3));
        colors.add(ContextCompat.getColor(context, R.color.aqua_5));
        //   colors.add(ContextCompat.getColor(context, R.color.aqua_6));
        colors.add(ContextCompat.getColor(context, R.color.aqua_7));
        colors.add(ContextCompat.getColor(context, R.color.aqua_8));
        //  colors.add(ContextCompat.getColor(context, R.color.aqua_9));
        colors.add(ContextCompat.getColor(context, R.color.aqua_10));
        colors.add(ContextCompat.getColor(context, R.color.green_1));
        colors.add(ContextCompat.getColor(context, R.color.green_2));
        // colors.add(ContextCompat.getColor(context, R.color.green_3));
        colors.add(ContextCompat.getColor(context, R.color.green_4));
        colors.add(ContextCompat.getColor(context, R.color.green_5));
        colors.add(ContextCompat.getColor(context, R.color.green_7));
        colors.add(ContextCompat.getColor(context, R.color.green_9));
        //  colors.add(ContextCompat.getColor(context, R.color.green_9));
        colors.add(ContextCompat.getColor(context, R.color.green_10));
        colors.add(ContextCompat.getColor(context, R.color.green_11));
        colors.add(ContextCompat.getColor(context, R.color.green_1));
        // colors.add(ContextCompat.getColor(context, R.color.silver_1));
        colors.add(ContextCompat.getColor(context, R.color.brown_1));
        colors.add(ContextCompat.getColor(context, R.color.brown_2));
        colors.add(ContextCompat.getColor(context, R.color.brown_3));
        // colors.add(ContextCompat.getColor(context, R.color.brown_4));

        colors.add(ContextCompat.getColor(context, R.color.edit_bototm_bg));
        return colors;
    }

    private ArrayList<Integer> morandni() {
        ArrayList<Integer> colors = new ArrayList<>();
        colors.add(ContextCompat.getColor(context, R.color.bg_gray_1));
        colors.add(ContextCompat.getColor(context, R.color.bg_gray_2));
        colors.add(ContextCompat.getColor(context, R.color.bg_gray_3));
        colors.add(ContextCompat.getColor(context, R.color.grey_900));
        colors.add(ContextCompat.getColor(context, R.color.bg_gray_4));
        colors.add(ContextCompat.getColor(context, R.color.bg_black));
        //  colors.add(ContextCompat.getColor(context, R.color.bg_plate_chestnut));
        colors.add(ContextCompat.getColor(context, R.color.bg_salmon));
        colors.add(ContextCompat.getColor(context, R.color.bg_vermilion));
        // colors.add(ContextCompat.getColor(context, R.color.bg_red_1));
        colors.add(ContextCompat.getColor(context, R.color.bg_red_2));
        colors.add(ContextCompat.getColor(context, R.color.bg_yellow_1));
        colors.add(ContextCompat.getColor(context, R.color.bg_yellow_2));
        //   colors.add(ContextCompat.getColor(context, R.color.bg_yellow_3));
        colors.add(ContextCompat.getColor(context, R.color.orange_1));
        // colors.add(ContextCompat.getColor(context, R.color.titan));
        colors.add(ContextCompat.getColor(context, R.color.flax));
        colors.add(ContextCompat.getColor(context, R.color.lemon));
        //  colors.add(ContextCompat.getColor(context, R.color.selective_yellow));
        colors.add(ContextCompat.getColor(context, R.color.pumpkin));
        //    colors.add(ContextCompat.getColor(context, R.color.pink_1));
        colors.add(ContextCompat.getColor(context, R.color.pink_2));
        colors.add(ContextCompat.getColor(context, R.color.pink_3));
        colors.add(ContextCompat.getColor(context, R.color.pink_4));
        //     colors.add(ContextCompat.getColor(context, R.color.pink_5));
        colors.add(ContextCompat.getColor(context, R.color.violet_1));
        colors.add(ContextCompat.getColor(context, R.color.violet_2));
        //   colors.add(ContextCompat.getColor(context, R.color.violet_3));
        colors.add(ContextCompat.getColor(context, R.color.violet_4));
        //   colors.add(ContextCompat.getColor(context, R.color.violet_5));
        colors.add(ContextCompat.getColor(context, R.color.blue_1));
        colors.add(ContextCompat.getColor(context, R.color.blue_2));
        colors.add(ContextCompat.getColor(context, R.color.blue_3));
        //  colors.add(ContextCompat.getColor(context, R.color.blue_4));
        colors.add(ContextCompat.getColor(context, R.color.blue_5));
        colors.add(ContextCompat.getColor(context, R.color.blue_6));
        //    colors.add(ContextCompat.getColor(context, R.color.blue_7));
        colors.add(ContextCompat.getColor(context, R.color.blue_8));
        //   colors.add(ContextCompat.getColor(context, R.color.blue_9));
        colors.add(ContextCompat.getColor(context, R.color.aqua_1));
        colors.add(ContextCompat.getColor(context, R.color.aqua_2));
        colors.add(ContextCompat.getColor(context, R.color.aqua_3));
        //colors.add(ContextCompat.getColor(context, R.color.aqua_5));
        colors.add(ContextCompat.getColor(context, R.color.aqua_6));
        //  colors.add(ContextCompat.getColor(context, R.color.aqua_7));
        colors.add(ContextCompat.getColor(context, R.color.aqua_8));
        colors.add(ContextCompat.getColor(context, R.color.aqua_9));
        // colors.add(ContextCompat.getColor(context, R.color.aqua_10));
        colors.add(ContextCompat.getColor(context, R.color.green_1));
        colors.add(ContextCompat.getColor(context, R.color.green_2));
        colors.add(ContextCompat.getColor(context, R.color.green_3));
        colors.add(ContextCompat.getColor(context, R.color.green_4));
        //colors.add(ContextCompat.getColor(context, R.color.green_5));
        colors.add(ContextCompat.getColor(context, R.color.green_7));
        colors.add(ContextCompat.getColor(context, R.color.green_9));
        colors.add(ContextCompat.getColor(context, R.color.green_9));
        colors.add(ContextCompat.getColor(context, R.color.green_10));
        //  colors.add(ContextCompat.getColor(context, R.color.green_11));
        colors.add(ContextCompat.getColor(context, R.color.green_1));
        colors.add(ContextCompat.getColor(context, R.color.silver_1));
        colors.add(ContextCompat.getColor(context, R.color.brown_1));
        // colors.add(ContextCompat.getColor(context, R.color.brown_2));
        colors.add(ContextCompat.getColor(context, R.color.brown_3));
        // colors.add(ContextCompat.getColor(context, R.color.brown_4));
        colors.add(ContextCompat.getColor(context, R.color.edit_bototm_bg));
        return colors;
    }
}
