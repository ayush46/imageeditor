package com.example.editor.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.editor.R
import com.example.editor.model.EditItemModel
import com.example.editor.viewholder.AdjustViewHolder

class AdjustAdapter(list : ArrayList<EditItemModel>) : RecyclerView.Adapter<AdjustViewHolder>(){

    private var dataList : ArrayList<EditItemModel> = list
    private var pos : Int = -1
    private var ctx : Context? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdjustViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val mView = inflater.inflate(R.layout.bottom_item_view, parent, false)
        ctx = mView.context
        return AdjustViewHolder(mView)
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onBindViewHolder(holder: AdjustViewHolder, position: Int) {
        val item = dataList.get(position)
        if(pos == position) {
            holder.tvLabel?.setTextColor(ctx?.resources?.getColor(R.color.grey_100)!!)
        } else {
            holder.tvLabel?.setTextColor(ctx?.resources?.getColor(R.color.grey_700)!!)
        }
        item.let { holder.bindViewData(it.label, it.iconDrawable, position) }
    }

    fun notifySelectedItem(position : Int?) {
        pos = position!!
        notifyDataSetChanged()
    }

}