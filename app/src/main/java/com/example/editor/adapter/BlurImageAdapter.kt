package com.example.editor.adapter
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.editor.R
import com.example.editor.callback.OnMediaItemSelected
import com.example.editor.model.MediaStoreData
import com.example.editor.viewholder.ImageHolder
import kotlinx.android.synthetic.main.image_grid_layout.view.*

class BlurImageAdapter(listOfImages: List<MediaStoreData>, onMediaItemSelected: OnMediaItemSelected) : RecyclerView.Adapter<ImageHolder>() {
    private var listOfImages: List<MediaStoreData>? =null
    private var onMediaItemSelected: OnMediaItemSelected?=null
    init {
        this.listOfImages=listOfImages
        this.onMediaItemSelected=onMediaItemSelected
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageHolder {
        return ImageHolder(LayoutInflater.from(parent.context).inflate(R.layout.image_grid_layout,parent,false),listOfImages,onMediaItemSelected)
    }

    override fun getItemCount(): Int {
        return listOfImages!!.size
    }

    override fun onBindViewHolder(holder: ImageHolder, position: Int) {
        Glide.with(holder.itemView.context).load(listOfImages!![position].uri).centerCrop().into(holder.itemView.img_image)
    }
}