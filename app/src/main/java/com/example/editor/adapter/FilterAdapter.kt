package com.example.editor.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.editor.R
import com.example.editor.viewholder.FilterViewHolder
import com.zomato.photofilters.utils.ThumbnailItem

class FilterAdapter(list : ArrayList<ThumbnailItem>?) : RecyclerView.Adapter<FilterViewHolder>(){

    private var dataList : ArrayList<ThumbnailItem>? = list

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FilterViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val mView = inflater.inflate(R.layout.filter_item_view, parent, false)
        return FilterViewHolder(mView)
    }

    override fun getItemCount(): Int {
        return dataList?.size!!
    }

    override fun onBindViewHolder(holder: FilterViewHolder, position: Int) {
        val filterItem = dataList?.get(position)
        filterItem.let { holder.bindViewData(it?.image, it?.filter) }
    }

}