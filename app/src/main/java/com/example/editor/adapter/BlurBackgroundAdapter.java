package com.example.editor.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.editor.R;

import java.util.ArrayList;

import static com.example.editor.utils.Utils.getResizedBitmap;

public class BlurBackgroundAdapter extends RecyclerView.Adapter<BlurBackgroundAdapter.BlurHolder> {
    private View mView;
    private Context context;
    private ClickOnBlurBackGround clickOnBlurBackGround;
    private   ArrayList<Bitmap> bitmaps=new ArrayList<>();
    private Bitmap bitmap,bitmapSavedLastTimeClick;
    private LoadBlur loadBlur;
    public int pos, setImagePos,lastTimePos;
    public BlurBackgroundAdapter(Context context,ClickOnBlurBackGround clickOnBlurBackGround,Bitmap bitmap) {
        this.clickOnBlurBackGround=clickOnBlurBackGround;
        this.context=context;
        this.bitmap= getResizedBitmap(bitmap,400);
        loadBlur=new LoadBlur();
        loadBlur.execute(this.bitmap);
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = getResizedBitmap(bitmap,400);
        loadBlur=new LoadBlur();
        loadBlur.execute(this.bitmap);
       notifyDataSetChanged();
    }

    @NonNull
    @Override
    public BlurHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mView= LayoutInflater.from(parent.getContext()).inflate(R.layout.image_blur_background,parent,false);
        return new BlurHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull BlurHolder holder, int position) {

        if (position == 0) {
            holder.blur.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            holder.blur.setImageResource(R.drawable.ic_block_white_24dp);
        }else if (position == 1) {
            if (setImagePos ==-2) {
                holder.blur.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                Drawable drawable = new BitmapDrawable(context.getResources(), bitmap);
                holder.blur.setBackground(drawable);
                holder.blur.setImageResource(R.drawable.ic_delete_white_24dp);
            }else {
                holder.blur.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                holder.blur.setImageResource(R.drawable.ic_gallery);
                holder.blur.setBackground(null);
            }
        } else {
            holder.blur.setScaleType(ImageView.ScaleType.FIT_XY);
            holder.blur.setImageBitmap(bitmaps.get(position));
        }
        if(position==pos){
            holder.layout.setBackgroundResource(R.drawable.ring_shape);
        }else {
            holder.layout.setBackgroundResource(0);

        }
    }

    public void setImage(int pos){
        this.setImagePos =pos;
        notifyDataSetChanged();
    }

    public void setVisibilityGoneRingShapeView(int pos){
            this.pos=pos;
            notifyDataSetChanged();
    }

    public int getLastTimePos(){
        return  lastTimePos;
    }
    @Override
    public int getItemCount() {
        return bitmaps.size();
    }
    void setPosition(int p){
        pos=p;
        notifyDataSetChanged();
    }

    public class BlurHolder extends RecyclerView.ViewHolder{
       private ImageView blur;
       private ImageView layout;
        public BlurHolder(@NonNull View itemView) {
            super(itemView);
            blur=itemView.findViewById(R.id.blurImageView);
            layout=itemView.findViewById(R.id.blur_layout);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(getAdapterPosition()==0 ){
                        clickOnBlurBackGround.onClickBlurBackGroundItem(getAdapterPosition());
                        setPosition(getAdapterPosition());

                    }else if(getAdapterPosition()==1){
                        clickOnBlurBackGround.onClickBlurBackGroundItem(getAdapterPosition());
                    }
                    if(clickOnBlurBackGround!=null && bitmaps!=null && getAdapterPosition()>1) {
                        clickOnBlurBackGround.onClickBlurBackGroundItem(bitmaps.get(getAdapterPosition()));
                        if(setImagePos!=-2) {
                            bitmapSavedLastTimeClick = bitmaps.get(getAdapterPosition());
                            lastTimePos=getAdapterPosition();
                        }
                        setPosition(getAdapterPosition());
                    }
                }
            });
        }
    }
    public Bitmap getBitmap(){
     return bitmapSavedLastTimeClick;
    }

    public interface ClickOnBlurBackGround{
        void onClickBlurBackGroundItem(Bitmap bitmap);
        void onClickBlurBackGroundItem(int pos);
    }

   private void setBitmapDataList() {
        if(bitmaps!=null && bitmaps.size()!=0) {
            bitmaps.clear();
        }
        bitmaps.add(blurBackground(bitmap, 1, 2));
        bitmaps.add(blurBackground(bitmap, 1, 2));
        bitmaps.add(blurBackground(bitmap, 1, 2));
        bitmaps.add(blurBackground(bitmap, 1, 10));
        bitmaps.add(blurBackground(bitmap, 1, 20));
        bitmaps.add(blurBackground(bitmap, 1, 40));
        bitmaps.add(blurBackground(bitmap, 1, 70));
        bitmaps.add(blurBackground(bitmap, 1, 100));
    }

    private class LoadBlur extends AsyncTask<Bitmap,Void,Bitmap>{

        @Override
        protected Bitmap doInBackground(Bitmap... bitmaps) {
            setBitmapDataList();
            return  null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            notifyDataSetChanged();


        }

    }
    public  Bitmap blurBackground(Bitmap sentBitmap, float scale, int radius) {

        if(bitmap==null){
            return null;
        }
        int width = Math.round(sentBitmap.getWidth() * scale);
        int height = Math.round(sentBitmap.getHeight() * scale);
        sentBitmap = Bitmap.createScaledBitmap(sentBitmap, width, height, false);

        Bitmap bitmap = sentBitmap.copy(sentBitmap.getConfig(), true);

        if (radius < 1) { return (null); } int w = bitmap.getWidth(); int h = bitmap.getHeight(); int[] pix = new int[w * h];
     //   Log.e("pix", w + " " + h + " " + pix.length);
        bitmap.getPixels(pix, 0, w, 0, 0, w, h); int wm = w - 1; int hm = h - 1; int wh = w * h; int div = radius + radius + 1; int r[] = new int[wh]; int g[] = new int[wh]; int b[] = new int[wh];
        int rsum, gsum, bsum, x, y, i, p, yp, yi, yw; int vmin[] = new int[Math.max(w, h)]; int divsum = (div + 1) >> 1;
        divsum *= divsum;
        int dv[] = new int[256 * divsum];
        for (i = 0; i < 256 * divsum; i++) {
            dv[i] = (i / divsum);
        }

        yw = yi = 0;

        int[][] stack = new int[div][3];
        int stackpointer;
        int stackstart;
        int[] sir;
        int rbs;
        int r1 = radius + 1;
        int routsum, goutsum, boutsum;
        int rinsum, ginsum, binsum;

        for (y = 0; y < h; y++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            for (i = -radius; i <= radius; i++) { p = pix[yi + Math.min(wm, Math.max(i, 0))]; sir = stack[i + radius]; sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);
                rbs = r1 - Math.abs(i);
                rsum += sir[0] * rbs;
                gsum += sir[1] * rbs;
                bsum += sir[2] * rbs;
                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }
            }
            stackpointer = radius;

            for (x = 0; x < w; x++) { r[yi] = dv[rsum]; g[yi] = dv[gsum]; b[yi] = dv[bsum]; rsum -= routsum; gsum -= goutsum; bsum -= boutsum;
                stackstart = stackpointer - radius + div; sir = stack[stackstart % div]; routsum -= sir[0]; goutsum -= sir[1]; boutsum -= sir[2];
                if (y == 0) { vmin[x] = Math.min(x + radius + 1, wm); } p = pix[yw + vmin[x]]; sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[(stackpointer) % div];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi++;
            }
            yw += w;
        }
        for (x = 0; x < w; x++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            yp = -radius * w;
            for (i = -radius; i <= radius; i++) { yi = Math.max(0, yp) + x; sir = stack[i + radius];
                sir[0] = r[yi]; sir[1] = g[yi]; sir[2] = b[yi]; rbs = r1 - Math.abs(i); rsum += r[yi] * rbs; gsum += g[yi] * rbs; bsum += b[yi] * rbs;
                if (i > 0) {
                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];
            } else {
                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];
            }

                if (i < hm) {
                    yp += w;
                }
            }
            yi = x;
            stackpointer = radius;
            for (y = 0; y < h; y++) {
                // Preserve alpha channel: ( 0xff000000 & pix[yi] )
                pix[yi] = ( 0xff000000 & pix[yi] ) | ( dv[rsum] << 16 ) | ( dv[gsum] << 8 ) | dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (x == 0) {
                    vmin[y] = Math.min(y + r1, hm) * w;
                }
                p = x + vmin[y];

                sir[0] = r[p];
                sir[1] = g[p];
                sir[2] = b[p];

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[stackpointer];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi += w;
            }
        }

        // Log.e("pix", w + " " + h + " " + pix.length);
        bitmap.setPixels(pix, 0, w, 0, 0, w, h);
        return (bitmap);
    }

}
