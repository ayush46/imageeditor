package com.example.editor.utils

import android.content.Context
import android.graphics.*
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import android.provider.OpenableColumns
import androidx.fragment.app.Fragment
import com.example.editor.R
import com.example.editor.constant.Constants
import com.example.editor.model.BottomNavModel
import com.example.editor.model.CropModel
import com.example.editor.model.EditItemModel
import com.example.editor.model.ImageFilterModel
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.isseiaoki.simplecropview.CropImageView
import ja.burhanrashid52.photoeditor.PhotoFilter
import java.io.File

class Utils {

    companion object{
        fun hideBottomSheetFragment(fragment : Fragment?){
            fragment.let {
                if (fragment?.isVisible!!) {
                    (fragment as BottomSheetDialogFragment).dismiss()
                }
            }
        }

        fun getBottomNavDataSet(context : Context) : ArrayList<BottomNavModel>{
            val dataSet = ArrayList<BottomNavModel>()
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                dataSet.add(BottomNavModel(context.getDrawable(R.drawable.ic_temp), "Canvas", Constants.EditToolType.CANVAS.name))
                dataSet.add(BottomNavModel(context.getDrawable(R.drawable.ic_filter), "Filter", Constants.EditToolType.FILTER.name))
                dataSet.add(BottomNavModel(context.getDrawable(R.drawable.ic_adjust), "Adjust", Constants.EditToolType.ADJUST.name))
                dataSet.add(BottomNavModel(context.getDrawable(R.drawable.ic_background), "Background", Constants.EditToolType.BACKGROUND.name))
                dataSet.add(BottomNavModel(context.getDrawable(R.drawable.ic_crop), "Crop", Constants.EditToolType.CROP.name))
                dataSet.add(BottomNavModel(context.getDrawable(R.drawable.ic_text), "Text", Constants.EditToolType.TEXT.name))
                dataSet.add(BottomNavModel(context.getDrawable(R.drawable.ic_rotate), "Rotate", Constants.EditToolType.ROTATE.name))
            }
            return dataSet
        }

        fun getFilterDataSet(imgPath : String?)  : ArrayList<ImageFilterModel>{
            val file = File(imgPath)
            val uri = Uri.fromFile(file)
            val mFilterImage = ArrayList<ImageFilterModel>()
            mFilterImage.add(ImageFilterModel(uri, PhotoFilter.NONE))
            mFilterImage.add(ImageFilterModel(uri, PhotoFilter.AUTO_FIX))
            mFilterImage.add(ImageFilterModel(uri, PhotoFilter.BRIGHTNESS))
            mFilterImage.add(ImageFilterModel(uri, PhotoFilter.CONTRAST))
            mFilterImage.add(ImageFilterModel(uri, PhotoFilter.DOCUMENTARY))
            mFilterImage.add(ImageFilterModel(uri, PhotoFilter.DUE_TONE))
            mFilterImage.add(ImageFilterModel(uri, PhotoFilter.FILL_LIGHT))
            mFilterImage.add(ImageFilterModel(uri, PhotoFilter.FISH_EYE))
            mFilterImage.add(ImageFilterModel(uri, PhotoFilter.GRAIN))
            mFilterImage.add(ImageFilterModel(uri, PhotoFilter.GRAY_SCALE))
            mFilterImage.add(ImageFilterModel(uri, PhotoFilter.LOMISH))
            mFilterImage.add(ImageFilterModel(uri, PhotoFilter.NEGATIVE))
            mFilterImage.add(ImageFilterModel(uri, PhotoFilter.POSTERIZE))
            mFilterImage.add(ImageFilterModel(uri, PhotoFilter.SATURATE))
            mFilterImage.add(ImageFilterModel(uri, PhotoFilter.SEPIA))
            mFilterImage.add(ImageFilterModel(uri, PhotoFilter.SHARPEN))
            mFilterImage.add(ImageFilterModel(uri, PhotoFilter.TEMPERATURE))
            mFilterImage.add(ImageFilterModel(uri, PhotoFilter.TINT))
            mFilterImage.add(ImageFilterModel(uri, PhotoFilter.VIGNETTE))
            mFilterImage.add(ImageFilterModel(uri, PhotoFilter.CROSS_PROCESS))
            mFilterImage.add(ImageFilterModel(uri, PhotoFilter.BLACK_WHITE))
            mFilterImage.add(ImageFilterModel(uri, PhotoFilter.FLIP_HORIZONTAL))
            mFilterImage.add(ImageFilterModel(uri, PhotoFilter.FLIP_VERTICAL))
//            mFilterImage.add(ImageFilterModel(uri, PhotoFilter.ROTATE))
            return mFilterImage
        }

        fun getCropModeDataSet() : ArrayList<CropModel> {
            val cropModeList = ArrayList<CropModel>()
            cropModeList.add(CropModel(1, 1, CropImageView.CropMode.FREE, "free"))
            cropModeList.add(CropModel(16, 9, CropImageView.CropMode.RATIO_16_9, "16:9"))
            cropModeList.add(CropModel(9, 16, CropImageView.CropMode.RATIO_9_16, "9:16"))
            cropModeList.add(CropModel(3, 4, CropImageView.CropMode.RATIO_3_4, "3:4"))
            cropModeList.add(CropModel(4, 3, CropImageView.CropMode.RATIO_4_3, "4:3"))
            cropModeList.add(CropModel(4, 4, CropImageView.CropMode.SQUARE, "Square"))
            cropModeList.add(CropModel(3, 3, CropImageView.CropMode.CIRCLE, "circle"))
            cropModeList.add(CropModel(2, 2, CropImageView.CropMode.CIRCLE_SQUARE, "circle square"))
            cropModeList.add(CropModel(5, 5, CropImageView.CropMode.FIT_IMAGE, "fit"))
            cropModeList.add(CropModel(7, 7, CropImageView.CropMode.CUSTOM, "custom"))

            return cropModeList
        }

        fun getAdjustDataSet(context: Context) : ArrayList<EditItemModel>{
            val adjustDataSet = ArrayList<EditItemModel>()
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                adjustDataSet.add(EditItemModel(Constants.AdjustItems.LIGHTNESS.name, context.getDrawable(R.drawable.ic_brightness)))
                adjustDataSet.add(EditItemModel(Constants.AdjustItems.CONTRAST.name, context.getDrawable(R.drawable.ic_contrast)))
                adjustDataSet.add(EditItemModel(Constants.AdjustItems.WRAMTH.name, context.getDrawable(R.drawable.ic_warmth)))
                adjustDataSet.add(EditItemModel(Constants.AdjustItems.SATURATION.name, context.getDrawable(R.drawable.ic_saturation)))
                adjustDataSet.add(EditItemModel(Constants.AdjustItems.FADE.name, context.getDrawable(R.drawable.ic_fade)))
                adjustDataSet.add(EditItemModel(Constants.AdjustItems.SHADOW.name, context.getDrawable(R.drawable.ic_shadow)))
                adjustDataSet.add(EditItemModel(Constants.AdjustItems.HIGHLIGHT.name, context.getDrawable(R.drawable.ic_highlight)))
            }
            return adjustDataSet
        }

        fun getRotateDataSet(context: Context) : ArrayList<EditItemModel>{
            val adjustDataSet = ArrayList<EditItemModel>()
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                adjustDataSet.add(EditItemModel(Constants.RotateItems.FLIP.name, context.getDrawable(R.drawable.ic_flip)))
                adjustDataSet.add(EditItemModel(Constants.RotateItems.ROTATE.name, context.getDrawable(R.drawable.ic_rotate)))
                adjustDataSet.add(EditItemModel(Constants.RotateItems.ANGEL.name, context.getDrawable(R.drawable.ic_angle)))
                adjustDataSet.add(EditItemModel(Constants.RotateItems.ZOOM.name, context.getDrawable(R.drawable.ic_zoom)))
            }
            return adjustDataSet
        }

        fun changeSaturation(src: Bitmap, saturation: Float): Bitmap {

            val w = src.width
            val h = src.height

            val bitmapResult = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888)
            val canvasResult = Canvas(bitmapResult)
            val paint = Paint()
            val colorMatrix = ColorMatrix()
            colorMatrix.setSaturation(saturation)
            val filter = ColorMatrixColorFilter(colorMatrix)
            paint.colorFilter = filter
            canvasResult.drawBitmap(src, 0f, 0f, paint)

            return bitmapResult
        }

        fun changeTemperature(src: Bitmap, temp: Float): Bitmap {

            val w = src.width
            val h = src.height

            val bitmapResult = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888)
            val canvasResult = Canvas(bitmapResult)
            val paint = Paint()
            val colorMatrix = ColorMatrix()

            colorMatrix.set(floatArrayOf(1f, 0f, 0f, temp, 0f, 0f, 1f, 0f, temp / 2, 0f, 0f, 0f, 1f, temp / 4, 0f, 0f, 0f, 0f, 1f, 0f))
            val filter = ColorMatrixColorFilter(colorMatrix)
            paint.colorFilter = filter
            canvasResult.drawBitmap(src, 0f, 0f, paint)

            return bitmapResult
        }

        fun changeBrightness(src: Bitmap, brightness: Float, contrast: Float): Bitmap {
            val alteredBitmap = Bitmap.createBitmap(src.width, src.height, src.config)
            val canvas = Canvas(alteredBitmap)
            val paint = Paint()
            val cm = ColorMatrix()
            cm.set(floatArrayOf(contrast, 0f, 0f, 0f, brightness, 0f, contrast, 0f, 0f, brightness, 0f, 0f, contrast, 0f, brightness, 0f, 0f, 0f, 1f, 0f))

            paint.colorFilter = ColorMatrixColorFilter(cm)
            val matrix = Matrix()
            canvas.drawBitmap(src, matrix, paint)

            return alteredBitmap

        }

        fun getImagePathFromUri(context: Context, contentUri: Uri): String? { /* This method can also finds hidden image path from its uri */
            val proj = arrayOf(MediaStore.Images.Media.DATA)
            val returnCursor = context.contentResolver.query(contentUri, proj, null, null, null)
            var column_index = 0
            var path: String?
            try {
                if (returnCursor != null) {
                    column_index = returnCursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
                    returnCursor.moveToFirst()
                }
                path = returnCursor!!.getString(column_index)
                returnCursor.close()
            } catch (e: IllegalArgumentException) { /*If path not found from query then search it in hidden images*/
                val receiveImageFileName = getFileNameFromUri(context, contentUri)
                path = getFilePathForHiddenImage(context, receiveImageFileName)
                e.printStackTrace()
            }
            return path
        }

        private  fun getFilePathForHiddenImage(context: Context, fileName: String?): String? {
            val FILE_TYPE_NO_MEDIA = ".nomedia"
            // Scan all no Media files
            val nonMediaCondition = (MediaStore.Files.FileColumns.MEDIA_TYPE
                    + "=" + MediaStore.Files.FileColumns.MEDIA_TYPE_NONE)
            // Files with name contain .nomedia
            val where = (nonMediaCondition + " AND "
                    + MediaStore.Files.FileColumns.TITLE + " LIKE ?")
            val params = arrayOf("%$FILE_TYPE_NO_MEDIA%")
            val cursor = context.contentResolver.query(
                    MediaStore.Files.getContentUri("external"), arrayOf(MediaStore.Files.FileColumns.DATA), where,
                    params, null)
            if (cursor != null) {
                while (cursor != null && cursor.moveToNext()) {
                    val hiddenFilePath = cursor.getString(cursor.getColumnIndex(MediaStore.Files.FileColumns.DATA))
                    if (hiddenFilePath != null) {
                        val hidenFile = File(hiddenFilePath)
                        if (hidenFile.exists()) {
                            val parentFile = hidenFile.parentFile
                            if (parentFile.isDirectory) {
                                val videoFileArray = parentFile.listFiles()
                                if (videoFileArray != null && videoFileArray.size > 0) {
                                    for (file in videoFileArray) {
                                        if (file.absolutePath.contains(fileName!!)) {
                                            return file.absolutePath
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return ""
        }

        private fun getFileNameFromUri(context: Context, uri: Uri): String? {
            var result: String? = null
            val scheme = uri.scheme
            if (scheme == "file") {
                result = uri.lastPathSegment
            } else if (uri.scheme == "content") {
                val cursor = context.contentResolver.query(uri, null, null, null, null)
                try {
                    if (cursor != null && cursor.moveToFirst()) {
                        result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME))
                    }
                } finally {
                    cursor!!.close()
                }
            }
            if (result == null) {
                result = uri.path
                val cut = result!!.lastIndexOf('/')
                if (cut != -1) {
                    result = result.substring(cut + 1)
                }
            }
            return result
        }

        @JvmStatic
        fun getResizedBitmap(image: Bitmap?, maxSize: Int): Bitmap? {
            var width = 0
            var height = 0
            var bitmap: Bitmap? = null
            if (image != null) {
                width = image.width
                height = image.height
                val bitmapRatio = width.toFloat() / height.toFloat()
                if (bitmapRatio > 1) {
                    width = maxSize
                    height = (width / bitmapRatio).toInt()
                } else {
                    height = maxSize
                    width = (height * bitmapRatio).toInt()
                }
                bitmap = Bitmap.createScaledBitmap(image, width, height, true)
            }
            return bitmap
        }

    }

}