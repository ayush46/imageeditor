package com.example.editor.utils

class AspectRatio(val width: Int?, val height: Int?, val name: String?) {

    fun getRatio(): Float {
        return height?.let { width?.toFloat()?.div(it) }!!
    }

    fun isSquare(): Boolean {
        return width == height
    }
}