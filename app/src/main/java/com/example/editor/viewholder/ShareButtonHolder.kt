package com.example.editor.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.editor.adapter.ShareButtonAdapter
import java.util.*

class ShareButtonHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    constructor(itemView: View, buttonList: ArrayList<ShareButtonAdapter.ButtonData>, onClickShareButton: ShareButtonAdapter.OnClickShareButton?) : this(itemView){
        itemView.setOnClickListener{
            onClickShareButton?.onClickShareButton(buttonName = buttonList[adapterPosition].buttonName)
        }
    }

}