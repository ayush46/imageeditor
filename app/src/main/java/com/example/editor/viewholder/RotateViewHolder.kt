package com.example.editor.viewholder

import android.graphics.drawable.Drawable
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.editor.R
import com.example.editor.event.RotateItemEvent

class RotateViewHolder(view : View) : RecyclerView.ViewHolder(view) {

    private var ivType : ImageView? = null
    var tvLabel : TextView? = null

    init {
        tvLabel = view.findViewById(R.id.tv_option)
        ivType = view.findViewById(R.id.iv_option)
    }

    fun bindViewData(label: String?, imgType: Drawable?, pos : Int?) {
        ivType?.setImageDrawable(imgType)
        tvLabel?.text = label

        ivType?.setOnClickListener {
            val rotateItemEvent = RotateItemEvent(label, pos)
            rotateItemEvent.post()
        }
    }
}