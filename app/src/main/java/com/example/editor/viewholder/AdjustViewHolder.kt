package com.example.editor.viewholder

import android.graphics.drawable.Drawable
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.editor.R
import com.example.editor.event.AdjustItemEvent

class AdjustViewHolder(view : View) : RecyclerView.ViewHolder(view) {

    var tvLabel : TextView? = null
    private var ivType : ImageView? = null

    init {
        tvLabel = view.findViewById<TextView>(R.id.tv_option)
        ivType = view.findViewById<ImageView>(R.id.iv_option)
    }

    fun bindViewData(label: String?, imgType: Drawable?, pos : Int?) {
        ivType?.setImageDrawable(imgType)
        tvLabel?.text = label
        ivType?.setOnClickListener {
            val adjustItemEvent = AdjustItemEvent(label, pos)
            adjustItemEvent.post()
        }
    }
}