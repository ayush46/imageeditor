package com.example.editor.viewholder

import android.net.Uri
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.editor.callback.OnMediaItemSelected
import com.example.editor.model.MediaStoreData

class ImageHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    constructor(itemView: View, listOfImages: List<MediaStoreData>?, onMediaItemSelected: OnMediaItemSelected?) : this(itemView){
        itemView.setOnClickListener {
            var uri: Uri?= Uri.parse(listOfImages?.get(adapterPosition)?.uri)
            if (uri != null) {
                onMediaItemSelected?.onMediaImageItemSelected(uri = uri)
            }
        }
    }
}