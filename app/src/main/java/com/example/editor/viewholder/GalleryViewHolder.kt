package com.example.editor.viewholder

import android.view.View
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.editor.R
import com.example.editor.event.GalleryImageEvent


class GalleryViewHolder(view : View) : RecyclerView.ViewHolder(view) {

    private var ivPhoto : ImageView? = null

    init {
        ivPhoto = view.findViewById<ImageView>(R.id.iv_photo)
    }

    fun bindViewData(imagePath: String) {
        Glide.with(ivPhoto?.context!!).load(imagePath).into(ivPhoto!!)
        ivPhoto?.setOnClickListener {
            val galleryImageEvent = GalleryImageEvent(imagePath)
            galleryImageEvent.post()
        }
    }

}