package com.example.editor.viewholder

import android.graphics.Bitmap
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.editor.R
import com.example.editor.event.FilterImageEvent
import com.zomato.photofilters.imageprocessors.Filter

class FilterViewHolder(view : View) : RecyclerView.ViewHolder(view) {

    private var tvOption : TextView? = null
    private var ivFilter : ImageView? = null

    init {
        tvOption = view.findViewById(R.id.tv_option)
        ivFilter = view.findViewById(R.id.iv_filter)
    }

    fun bindViewData(bitmap: Bitmap?, filter: Filter?) {
        ivFilter?.setImageBitmap(bitmap)
        tvOption?.text = filter?.name
        ivFilter?.setOnClickListener {
            val filterImageEvent = FilterImageEvent(filter)
            filterImageEvent.post()
        }
    }
}