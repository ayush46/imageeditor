package com.example.editor.viewholder

import android.graphics.drawable.Drawable
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.editor.R
import com.example.editor.constant.Constants
import com.example.editor.event.*

class BottomNavViewHolder(view : View) : RecyclerView.ViewHolder(view) {

    private var tvOption : TextView? = null
    private var ivOption : ImageView? = null

    init {
        tvOption = view.findViewById(R.id.tv_option)
        ivOption = view.findViewById(R.id.iv_option)
    }

    fun bindViewData(drawable : Drawable?, text : String?, toolType : String?) {
        tvOption?.text = text?.toUpperCase()
        ivOption?.setImageDrawable(drawable)
        ivOption?.setOnClickListener {
            when(toolType) {
                Constants.EditToolType.FILTER.name -> {
                    val filterEvent = FilterEvent()
                    filterEvent.post()
                }
                Constants.EditToolType.ADJUST.name -> {
                    val adjustEvent = AdjustEvent()
                    adjustEvent.post()
                }
                Constants.EditToolType.CROP.name -> {
                    val cropEvent = CropEvent()
                    cropEvent.post()
                }
                Constants.EditToolType.ROTATE.name -> {
                    val rotateEvent = RotateEvent()
                    rotateEvent.post()
                }
                Constants.EditToolType.BACKGROUND.name -> {
                    val backgroundEvent = BackgroundEvent()
                    backgroundEvent.post()
                }
            }
        }
    }

}