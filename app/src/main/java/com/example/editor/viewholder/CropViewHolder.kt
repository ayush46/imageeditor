package com.example.editor.viewholder

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.editor.R
import com.example.editor.event.CropModeEvent
import com.example.editor.model.CropModel
import com.example.editor.utils.AspectRatio
import com.example.editor.view.AspectRatioPreviewView
import com.isseiaoki.simplecropview.CropImageView

class CropViewHolder(view: View) : RecyclerView.ViewHolder(view)  {

    private var tvLabel : TextView? = null
    private var aspectRatio : AspectRatioPreviewView? = null

    init {
        tvLabel = view.findViewById(R.id.tv_ratio)
        aspectRatio = view.findViewById(R.id.aspect_ratio_preview)
    }

    fun bindData(model: CropModel?, selectedCropMode: CropImageView.CropMode) {
        val mode = model?.cropMode
        tvLabel?.text = model?.modeName
        aspectRatio?.setAspectRatio(AspectRatio(model?.width, model?.height, mode?.name))
        if(selectedCropMode.equals(model?.cropMode)) {
            aspectRatio?.isSelected = true
        } else {
            aspectRatio?.isSelected = false
        }
        aspectRatio?.setOnClickListener {
            aspectRatio?.isSelected = true
            val cropModeEvent = CropModeEvent(mode)
            cropModeEvent.post()
        }
    }

}