package com.example.editor.viewholder

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.editor.R
import com.example.editor.event.GalleryCategoryEvent
import java.util.*

class GalleryCategoryViewHolder(view : View) : RecyclerView.ViewHolder(view) {

    private var tvCategory : TextView? = null
    private var ivCategory : ImageView? = null
    private var tvCount : TextView? = null

    init {
        tvCategory = view.findViewById(R.id.tv_cat)
        ivCategory = view.findViewById(R.id.iv_cat)
        tvCount = view.findViewById(R.id.tv_count)
    }

    fun bindData(category: String, imageList: ArrayList<String>?) {
        Glide.with(ivCategory?.context!!).load(imageList?.get(0)).into(ivCategory!!)
        tvCategory?.text = category
        tvCount?.text = imageList?.size.toString()
        tvCategory?.setOnClickListener {
            val galleryCategoryEvent = GalleryCategoryEvent(category, imageList)
            galleryCategoryEvent.post()
        }
    }

}