package com.example.editor.event

import com.example.editor.constant.Constants
import com.isseiaoki.simplecropview.CropImageView

class CropModeEvent(val cropMode : CropImageView.CropMode?) : Event(Constants.CropEvent.CROP_MODE_SELECTION.name)