package com.example.editor.event

import com.example.editor.constant.Constants

class CropEvent : Event(Constants.CropEvent.CROP_EVENT.name)