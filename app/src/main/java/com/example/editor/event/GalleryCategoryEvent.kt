package com.example.editor.event

import com.example.editor.constant.Constants

class GalleryCategoryEvent(val category : String, val imageList : ArrayList<String>?) : Event(Constants.GalleryEvent.GALLERY_CATEGORY_SELECTION_EVENT.name)