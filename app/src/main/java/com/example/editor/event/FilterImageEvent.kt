package com.example.editor.event

import com.example.editor.constant.Constants
import com.zomato.photofilters.imageprocessors.Filter

class FilterImageEvent(val filter : Filter?) : Event(Constants.FilterEvent.FILTER_ITEM_SELECTION.name)