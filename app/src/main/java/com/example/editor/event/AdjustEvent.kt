package com.example.editor.event

import com.example.editor.constant.Constants

class AdjustEvent : Event(Constants.AdjustEvent.ADJUST_EVENT.name)