package com.example.editor.event

import com.example.editor.constant.Constants

class FilterEvent : Event(Constants.FilterEvent.FILTER_EVENT.name)