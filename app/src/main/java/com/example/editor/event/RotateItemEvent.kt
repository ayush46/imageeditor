package com.example.editor.event

import com.example.editor.constant.Constants

class RotateItemEvent(val rotateType : String?, val pos : Int?) : Event(Constants.RotateEvent.ROTATE_ITEM_SELECTION.name)