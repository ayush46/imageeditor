package com.example.editor.event

import com.example.editor.constant.Constants

class GalleryImageEvent(val imagePath : String) : Event(Constants.GalleryEvent.GALLERY_IMAGE_SELECTION_EVENT.name)