package com.example.editor.event

import com.example.editor.constant.Constants

class AdjustItemEvent(val adjustType : String?, val position : Int?) : Event(Constants.AdjustEvent.ADJUST_ITEM_SELECTION.name)