package com.example.editor.event;

import static org.greenrobot.eventbus.EventBus.getDefault;

public class EventBus implements IEventBus {

    private static EventBus mInstance;
    private org.greenrobot.eventbus.EventBus mEventBus;

    private EventBus() {
        mEventBus = getDefault();
    }

    @Override
    public void register(Object subscriber) {
        if (!mEventBus.isRegistered(subscriber)) {
            mEventBus.register(subscriber);
        }
    }

    @Override
    public void unregister(Object subscriber) {
        if (mEventBus.isRegistered(subscriber)) {
            mEventBus.unregister(subscriber);
        }
    }

    public static EventBus getInstance() {
        if (mInstance == null) {
            mInstance = new EventBus();
        }
        return mInstance;
    }

    @Override
    public <T extends Event> void postEvent(T event) {
        mEventBus.post(event);
    }
}
