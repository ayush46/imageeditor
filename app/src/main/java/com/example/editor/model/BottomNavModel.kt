package com.example.editor.model

import android.graphics.drawable.Drawable
import java.io.Serializable

data class BottomNavModel (
        val imageDrawable : Drawable?,
        val title : String?,
        val type : String?) : Serializable
