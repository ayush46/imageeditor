package com.example.editor.model

import android.net.Uri
import ja.burhanrashid52.photoeditor.PhotoFilter
import java.io.Serializable

data class ImageFilterModel (
        var uri: Uri?,
        val filter : PhotoFilter?) : Serializable


