package com.example.editor.model

import com.isseiaoki.simplecropview.CropImageView
import java.io.Serializable

data class CropModel (
        val width : Int,
        val height : Int,
        val cropMode : CropImageView.CropMode,
        val modeName : String) : Serializable