package com.example.editor.model

import android.graphics.drawable.Drawable
import java.io.Serializable

data class EditItemModel (
        val label: String?,
        val iconDrawable : Drawable?) : Serializable
